//
//  CustomerBasket.m
//  Kokzhiek
//
//  Created by Admin on 05.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "CustomerBasket.h"
#import "DeliveryType.h"
#import "IQDropDownTextField.h"
#import "MBProgressHUD.h"

@interface CustomerBasket ()

@end

@implementation CustomerBasket

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.txtFio.delegate = self;
    self.txtEmail.delegate = self;
    self.txtPhone.delegate = self;
    //self.txtCity.delegate = self;
    self.ddlCity.isOptionalDropDown = NO;
    [self.ddlCity setItemList:[NSArray arrayWithObjects: [NSString stringWithFormat:@"%@ *",NSLocalizedString(@"CITY", @"")],
                               NSLocalizedString(@"CITY_1", @""),NSLocalizedString(@"CITY_2", @""),NSLocalizedString(@"CITY_3", @""),NSLocalizedString(@"CITY_4", @""),NSLocalizedString(@"CITY_5", @""),
                               NSLocalizedString(@"CITY_6", @""),NSLocalizedString(@"CITY_7", @""),NSLocalizedString(@"CITY_8", @""),NSLocalizedString(@"CITY_9", @""),NSLocalizedString(@"CITY_10", @""),
                               NSLocalizedString(@"CITY_11", @""),NSLocalizedString(@"CITY_12", @""),NSLocalizedString(@"CITY_13", @""),NSLocalizedString(@"CITY_14", @""),NSLocalizedString(@"CITY_15", @""),NSLocalizedString(@"CITY_16", @""),NSLocalizedString(@"CITY_17", @""),nil]];
    
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *bi1 = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"CLOSE", @"") style:UIBarButtonItemStylePlain target:self action:@selector(barButtonCustomPressed:)];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *bi2 = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"READY", @"") style:UIBarButtonItemStylePlain target:self action:@selector(barButtonCustomPressed:)];
                            
    //UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneBtnPressToGetValue)];
    
    [toolBar setItems:[[NSArray alloc] initWithObjects:bi1,spacer,bi2,nil]];
    [toolBar setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
    self.ddlCity.inputAccessoryView = toolBar;
    
    
    
    
    
    NSString*name=[[NSUserDefaults standardUserDefaults]objectForKey:@"name"];
    NSString*phone=[[NSUserDefaults standardUserDefaults]objectForKey:@"phone"];
    NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    
    self.txtFio.text = name;
    self.txtPhone.text = phone;
    self.txtEmail.text = email;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)barButtonCustomPressed:(UIBarButtonItem*)btn
{
    [self.view endEditing:true];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField==self.txtFio){
        [self.txtEmail becomeFirstResponder];
    }
    else   if (theTextField==self.txtEmail){
        [self.txtPhone becomeFirstResponder];
    }
    else  {
        [theTextField resignFirstResponder];
    }
    return YES;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    Boolean b = YES;
    
    if([identifier isEqualToString:@"delivery"]){
        if([self.txtFio.text isEqualToString:@""]){
            b = NO;
        }
        if([self.txtPhone.text isEqualToString:@""]){
            b = NO;
        }
        if([self.txtEmail.text isEqualToString:@""]){
            b = NO;
        }
        
        
        if([self.ddlCity.selectedItem isEqualToString:[NSString stringWithFormat:@"%@ *",NSLocalizedString(@"CITY", @"")]]){
            b = NO;
        }
    }
    
    if(b == NO){
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = NSLocalizedString(@"PLEASE_FILL_IN", @"");
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        
        [hud hide:YES afterDelay:2];
    }
    
    return b;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"delivery"]) {
        
        NSString *d = self.ddlCity.selectedItem;
        if([d isEqualToString:NSLocalizedString(@"CITY_1", @"")]){
            d = @"1";
        }else if([d isEqualToString:NSLocalizedString(@"CITY_2", @"")]){
            d = @"2";
        }else if([d isEqualToString:NSLocalizedString(@"CITY_3", @"")]){
            d = @"3";
        }else if([d isEqualToString:NSLocalizedString(@"CITY_4", @"")]){
            d = @"4";
        }else if([d isEqualToString:NSLocalizedString(@"CITY_5", @"")]){
            d = @"5";
        }else if([d isEqualToString:NSLocalizedString(@"CITY_6", @"")]){
            d = @"6";
        }else if([d isEqualToString:NSLocalizedString(@"CITY_7", @"")]){
            d = @"7";
        }else if([d isEqualToString:NSLocalizedString(@"CITY_8", @"")]){
            d = @"8";
        }else if([d isEqualToString:NSLocalizedString(@"CITY_9", @"")]){
            d = @"9";
        }else if([d isEqualToString:NSLocalizedString(@"CITY_10", @"")]){
            d = @"10";
        }else if([d isEqualToString:NSLocalizedString(@"CITY_11", @"")]){
            d = @"11";
        }else if([d isEqualToString:NSLocalizedString(@"CITY_12", @"")]){
            d = @"12";
        }else if([d isEqualToString:NSLocalizedString(@"CITY_13", @"")]){
            d = @"13";
        }else if([d isEqualToString:NSLocalizedString(@"CITY_14", @"")]){
            d = @"14";
        }else if([d isEqualToString:NSLocalizedString(@"CITY_15", @"")]){
            d = @"15";
        }else if([d isEqualToString:NSLocalizedString(@"CITY_16", @"")]){
            d = @"16";
        }else if([d isEqualToString:NSLocalizedString(@"CITY_17", @"")]){
           d = @"17";
        }else{
           d = @"";
        }
        
        DeliveryType *vc=segue.destinationViewController;
        vc.txtFio = self.txtFio.text;
        vc.txtEmail = self.txtEmail.text;
        vc.txtPhone = self.txtPhone.text;
        vc.txtCity = d;
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
    }
}
@end
