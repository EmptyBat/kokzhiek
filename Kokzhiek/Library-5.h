//
//  Library-5.h
//  Kokzhiek
//
//  Created by bugingroup on 07.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLSwipeContainerController.h"
@interface Library_5 : UICollectionViewController<XLSwipeContainerChildItem,UICollectionViewDataSource,UICollectionViewDelegate>

@end
