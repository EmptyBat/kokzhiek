//
//  MyBooks.m
//  Kokzhiek
//
//  Created by bugingroup on 13.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "MyBooks.h"

#import "AFNetworking.h"
#import <TLYShyNavBar/TLYShyNavBarManager.h>
#import "SWRevealViewController.h"
#import "SVProgressHUD.h"
#import "constants.h"
#import "ORGContainerCellView.h"
#import "ORGContainerCell.h"
#import "ORGArticleCollectionViewCell.h"
#import "BookDetail.h"
#import "EPubViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "BookRead.h"
#import "CollectionViewCell.h"
#import "mybookContainerCell.h"
#import "Reachability.h"
@interface MyBooks ()<MBProgressHUDDelegate> {
    MBProgressHUD *HUD;
    NSArray*book1;
    NSArray*book2;
    NSArray*book3;
    NSDictionary*BookDetails;
    NSURL *urlBook;
    Reachability *internetReachableFoo;
    bool internet_connection_status;
}

@property (strong) NSMutableArray *devices;
@end

@implementation MyBooks

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.view.frame = [[UIScreen mainScreen] bounds];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(BookDetail:)
                                                 name:@"BookDetail" object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MyBooks:) name:@"MyBooks" object:nil];
    self.shyNavBarManager.scrollView = self.tableView;
    [self.shyNavBarManager setStickyExtensionView:NO];
    [self.tableView registerClass:[mybookContainerCell class] forCellReuseIdentifier:@"cell"];
    _menu.target = self.revealViewController;
    _menu.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}
-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    internetReachableFoo = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    // Internet is reachable
    internetReachableFoo.reachableBlock = ^(Reachability*reach)
    {
        
        internet_connection_status = true;
        [self getData];
        
    };
    
    
    // Internet is not reachable
    internetReachableFoo.unreachableBlock = ^(Reachability*reach)
    {
        internet_connection_status = false;
        // Update the UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Books"];
            book2 = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
            NSLog(@"asfasffas %@",book2);
            [self.tableView reloadData];
        });
    };
    
    [internetReachableFoo startNotifier];
}
- (void)myProgressTask {
    float progress = 0.0f;
    while (progress < 1.0f) {
        progress += 0.01f;
        HUD.progress = progress;
        usleep(50000);
    }
}

- (void)BookDetail:(NSNotification *)notice{
    if (internet_connection_status == true){
    BookDetails = [notice object];
        NSLog(@"12323112321231  %@" ,BookDetails);
    
    NSInteger id = [[BookDetails objectForKey:@"id"] integerValue];
    Boolean readBook = NO;
    
    if([book2 isKindOfClass:[NSArray class]]) {
        for(NSDictionary *dic in book2){
            if([[dic objectForKey:@"id"] integerValue] == id){
                readBook = YES;
            }
        }
    
    }
    
    if([book3 isKindOfClass:[NSArray class]]) {
        for(NSDictionary *dic in book3){
            if([[dic objectForKey:@"id"] integerValue] == id){
                readBook = YES;
            }
        }
    }
    
    if(!readBook){
        [self performSegueWithIdentifier:@"BookDetail" sender:self];
    }
    }
    
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = [appDelegate managedObjectContext];
        NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Books" inManagedObjectContext:context];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDesc];
    NSPredicate *pred ;
    if (internet_connection_status == true)
    {
     pred = [NSPredicate predicateWithFormat:@"(book_id = %@)",[BookDetails objectForKey:@"id"]];
    }
    else {
         NSManagedObject *device = [notice object];
        pred = [NSPredicate predicateWithFormat:@"(book_id = %@)",[device valueForKey:@"book_id"]];
    }
        [request setPredicate:pred];
        NSManagedObject *matches = nil;
        NSError *error;
        NSArray *objects = [context executeFetchRequest:request error:&error];
        
        HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:HUD];
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        NSString*combine=[NSString stringWithFormat:@"http://www.kokzhiek.kz/books/%@",[BookDetails objectForKey:@"source"]];
        combine = [combine stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"++++9**sasasa*%@",BookDetails);
        NSURL *URL = [NSURL URLWithString:combine];
        NSLog(@"***++%@",URL);
         if ([objects count] == 0) {
        NSURLRequest *request = [NSURLRequest requestWithURL:URL];
        NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
            NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
            NSString*book_id=[NSString stringWithFormat:@"%@",[BookDetails objectForKey:@"id"]];
            return [documentsDirectoryURL URLByAppendingPathComponent:book_id];
        } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
            AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
            NSString *urlString = [filePath path];
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            NSManagedObject *newContact;
            newContact = [NSEntityDescription insertNewObjectForEntityForName:@"Books" inManagedObjectContext:context];
            NSString*book_id=[NSString stringWithFormat:@"%@",[BookDetails objectForKey:@"id"]];
            NSString*imagename=[NSString stringWithFormat:@"%@%@",KimageURL,[BookDetails objectForKey:@"image"]];
            
            NSURL *imageURL = [NSURL URLWithString:imagename];
            NSLog(@"***++1221%@",imageURL);
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
            dispatch_async(queue, ^{
                NSData *data = [NSData dataWithContentsOfURL:imageURL];
                UIImage *image = [UIImage imageWithData:data];
                dispatch_sync(dispatch_get_main_queue(), ^{
                      NSData *final_value = UIImageJPEGRepresentation(image, 1.0);
                    NSError *error;
                  [newContact setValue:final_value forKey:@"image"];
                    [newContact setValue:book_id forKey:@"book_id"];
                    [newContact setValue:urlString forKey:@"direct"];
                    [context save:&error];
                });
                
            });
            NSLog(@"File downloaded to: %@", filePath);
            self->urlBook = filePath;
            //[self performSegueWithIdentifier:@"read" sender:self];
            
            //[epubViewController loadEpub:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"tolstoy-war-and-peace" ofType:@"epub"]]];
            [[NSUserDefaults standardUserDefaults] setObject:urlString forKey:@"book_url"];
            NSLog(@"%@   asffsassaafs",self->urlBook);
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSString * storyboardName = @"Main";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
            UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"READER"];
            [self presentViewController:vc animated:YES completion:nil];
        }];
        [downloadTask resume];
        HUD.mode = MBProgressHUDModeDeterminateHorizontalBar;
        HUD.delegate = self;
        [HUD showWhileExecuting:@selector(myProgressTask) onTarget:self withObject:nil animated:YES];
         }
         else {
             NSLog(@"working here maybe not");
             matches = objects[0];
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
             NSManagedObject *device = [notice object];
            NSString*urlString= [NSString stringWithFormat:@"%@",[device valueForKey:@"direct"]];
             NSLog(@"book url ==  %@",urlString);
            
             [[NSUserDefaults standardUserDefaults] setObject:urlString forKey:@"book_url"];
             NSLog(@"%@   asffsassaafs",self->urlBook);
             [[NSUserDefaults standardUserDefaults] synchronize];
             NSString * storyboardName = @"Main";
             UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
             UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"READER"];
             [self presentViewController:vc animated:YES completion:nil];
        HUD.mode = MBProgressHUDModeDeterminateHorizontalBar;
        HUD.delegate = self;
        [HUD showWhileExecuting:@selector(myProgressTask) onTarget:self withObject:nil animated:YES];
                 }
}
//- (void)BookDetail:(NSNotification *)notice{
//    BookDetails = [notice object];
//
//
//    NSInteger id = [[BookDetails objectForKey:@"id"] integerValue];
//    Boolean readBook = NO;
//
//    if([book2 isKindOfClass:[NSArray class]]) {
//        for(NSDictionary *dic in book2){
//            if([[dic objectForKey:@"id"] integerValue] == id){
//                readBook = YES;
//            }
//        }
//    }
//
//    if(!readBook){
//        [self performSegueWithIdentifier:@"BookDetail" sender:self];
//    }else{
//
//
//        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
//        NSManagedObjectContext *context = [appDelegate managedObjectContext];
//        NSEntityDescription *entityDesc =
//        [NSEntityDescription entityForName:@"Books" inManagedObjectContext:context];
//        NSFetchRequest *request = [[NSFetchRequest alloc] init];
//        [request setEntity:entityDesc];
//        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(book_id = %@)",[BookDetails objectForKey:@"id"]];
//        [request setPredicate:pred];
//        NSManagedObject *matches = nil;
//        NSError *error;
//        NSArray *objects = [context executeFetchRequest:request error:&error];
//
//
//
//
//        if ([objects count] == 0) {
//            HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
//            [self.navigationController.view addSubview:HUD];
//            NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//            AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
//            NSString*combine=[NSString stringWithFormat:@"http://www.kokzhiek.kz/books/%@",[BookDetails objectForKey:@"source"]];
//            combine = [combine stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//            NSLog(@"++++9***%@",combine);
//            NSURL *URL = [NSURL URLWithString:combine];
//            NSLog(@"***++%@",URL);
//
//            NSURLRequest *request = [NSURLRequest requestWithURL:URL];
//            NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
//                NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
//                NSString*book_id=[NSString stringWithFormat:@"%@",[BookDetails objectForKey:@"id"]];
//                return [documentsDirectoryURL URLByAppendingPathComponent:book_id];
//            } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
//                AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
//                NSString *urlString = [filePath absoluteString];
//                NSManagedObjectContext *context = [appDelegate managedObjectContext];
//                NSManagedObject *newContact;
//                newContact = [NSEntityDescription insertNewObjectForEntityForName:@"Books" inManagedObjectContext:context];
//                NSString*book_id=[NSString stringWithFormat:@"%@",[BookDetails objectForKey:@"id"]];
//                //        NSString *img = self.image_url;
//                //        NSString*imagename=[NSString stringWithFormat:@"%@img/book_image/%@",kBaseURL,img];
//                //        NSURL *imageURL = [NSURL URLWithString:imagename];
//                NSString*imagename=[NSString stringWithFormat:@"%@%@",KimageURL,_image_url];
//                NSURL *imageURL = [NSURL URLWithString:imagename];
//                NSLog(@"***++%@",imageURL);
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
//
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [newContact setValue:imageData forKey:@"image"];
//                    });
//                });
//                [newContact setValue:book_id forKey:@"book_id"];
//                [newContact setValue:urlString forKey:@"direct"];
//                [context save:&error];
//
//                NSLog(@"File downloaded to: %@", filePath);
//
//                //                EPubViewController *epubViewController =  [[EPubViewController alloc]init];
//                //                [epubViewController loadEpub:filePath];
//                //                [self presentViewController:epubViewController animated:YES completion:^{
//                //                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//                //                }];
//
//                self->urlBook = filePath;
//                [self performSegueWithIdentifier:@"read" sender:self];
//            }];
//            [downloadTask resume];
//            HUD.mode = MBProgressHUDModeDeterminateHorizontalBar;
//            HUD.delegate = self;
//            [HUD showWhileExecuting:@selector(myProgressTask) onTarget:self withObject:nil animated:YES];
//        }else{
//            matches = objects[0];
//
//            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//            NSManagedObject *device = [self.devices objectAtIndex:0];
//            NSString*urlString= [NSString stringWithFormat:@"%@",[device valueForKey:@"direct"]];
//
//            NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//
//            self->urlBook = url;
//            [self performSegueWithIdentifier:@"read" sender:self];
//            //            EPubViewController *epubViewController =  [[EPubViewController alloc]init];
//            //            [epubViewController loadEpub:url];
//            //
//            //            [self presentViewController:epubViewController animated:YES completion:^{
//            //                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//            //            }];
//        }
//
//
//    }
//}
-(void)getData{
    NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    NSString*password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
    
    NSString*url=[NSString stringWithFormat:@"%@kk/mobile/mybooks?email=%@&password=%@",kBaseURL,email,password];
    NSLog(@"%@",url);
    [SVProgressHUD show];
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSArray *j = responseObject;
        if(j.count > 0 ){
            if([[responseObject objectForKey:@"saved_books"] isKindOfClass:[NSArray class]]){
                book1= [responseObject objectForKey:@"saved_books"];
            }
            if([[responseObject objectForKey:@"bought_books"] isKindOfClass:[NSArray class]]){
                book2= [responseObject objectForKey:@"bought_books"];
            }
            if([[responseObject objectForKey:@"gifted_books"] isKindOfClass:[NSArray class]]){
                book3= [responseObject objectForKey:@"gifted_books"];
            }
        }
        
        [SVProgressHUD dismiss];
        [self.tableView setHidden:NO];
        [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [SVProgressHUD dismiss];
    }];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(13, 0, 200, 30)];
    UIButton *more = [[UIButton alloc] initWithFrame:CGRectMake(tableView.frame.size.width-60, 0, 60, 30)];
    [more setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [label setFont:[UIFont boldSystemFontOfSize:14]];
    [more setTitle:NSLocalizedString(@"MORE", "") forState:UIControlStateNormal];
    [more.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
    more.hidden = YES;
    if (section == 0){
        [label setText:NSLocalizedString(@"BOOKS_SAVED", "")];
    }
    else if (section == 1){
        [label setText:NSLocalizedString(@"BOOKS_BOUGHT", "")];
    }
    else{
        [label setText:NSLocalizedString(@"BOOKS_GIFTED", "")];
    }
    [view addSubview:label];
    [view addSubview:more];
    [view setBackgroundColor:[UIColor lightTextColor]];
    return view;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0){
        mybookContainerCell*cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        [cell setCollectionData:book1];
        cell.userInteractionEnabled=YES;
        return cell;
    }
    else     if (indexPath.section==1){
        mybookContainerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
            [cell setCollectionData:book2];
            cell.userInteractionEnabled=YES;
        
        return cell;
    }
    else {
        mybookContainerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        [cell setCollectionData:book3];
        cell.userInteractionEnabled=YES;
        return cell;
    }
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"BookDetail"]) {
        
        BookDetail*Detail=segue.destinationViewController;
        Detail.image_url = [BookDetails objectForKey:@"image"];
        Detail.book_title = [BookDetails objectForKey:@"title"];
        Detail.author = [BookDetails objectForKey:@"author"];
        Detail.isbn = [BookDetails objectForKey:@"isbn"];
        Detail.description_book = [BookDetails objectForKey:@"description"];
        Detail.ebook_price = [BookDetails objectForKey:@"ebook_price"];
        Detail.price = [BookDetails objectForKey:@"price"];
        Detail.page = [BookDetails objectForKey:@"page"];
        Detail.publish_year = [BookDetails objectForKey:@"publish_year"];
        Detail.formats = [BookDetails objectForKey:@"formats"];
        Detail.book_id = [BookDetails objectForKey:@"id"];
        Detail.source =[BookDetails objectForKey:@"source"];
        Detail.status = [BookDetails objectForKey:@"status"];
        NSString *ii = [BookDetails valueForKey:@"id"];
        for (id b in book2) {
            NSString *yy = [b valueForKey:@"id"];
            
            if( ii == yy){
                Detail.demo = 1;
            }
        }
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
    }
    else if ([segue.identifier isEqualToString:@"read"]) {
        
        BookRead*vc=segue.destinationViewController;
        
        NSString* tl =[BookDetails objectForKey:@"title"];
        vc.tl =tl;
        vc.path = self->urlBook;
        //vc.path = @"tolstoy-war-and-peace";
        
        
        
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
    }
}
@end
