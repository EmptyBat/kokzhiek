//
//  DeliveryType.m
//  Kokzhiek
//
//  Created by Admin on 05.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "DeliveryType.h"
#import "KazPost.h"
#import "Kurier.h"
#import "Pickup.h"
#import "PaymentType.h"

@interface DeliveryType (){
    NSInteger s;
}

@end

@implementation DeliveryType

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIImage *img = [UIImage imageNamed:@"fon"];
    UIImageView *imgV = [[UIImageView alloc] initWithImage:img];
    imgV.contentMode = UIViewContentModeScaleAspectFill;
    self.tableView.backgroundView = imgV;
    
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.layoutMargins = UIEdgeInsetsZero;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    if ( [self.txtCity isEqualToString:@"2"]){
        self->s = 2;
    }
    else {
        self->s = 1;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self->s;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *simpleTableIdentifier = @"";
    
    if(self->s == 2){
        if(indexPath.row == 0){
            simpleTableIdentifier = @"cell1";
        }else {
            simpleTableIdentifier = @"cell2";
        }
            
    }else{
        simpleTableIdentifier = @"cell3";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    cell.layoutMargins = UIEdgeInsetsZero;
    cell.preservesSuperviewLayoutMargins = false;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"curier"]) {
        
        Kurier *vc=segue.destinationViewController;
        vc.txtFio = self.txtFio;
        vc.txtEmail = self.txtEmail;
        vc.txtPhone = self.txtPhone;
        vc.txtCity = self.txtCity;
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }else if ([segue.identifier isEqualToString:@"kazpost"]){
        KazPost *vc=segue.destinationViewController;
        vc.txtFio = self.txtFio;
        vc.txtEmail = self.txtEmail;
        vc.txtPhone = self.txtPhone;
        vc.txtCity = self.txtCity;
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }else{
        PaymentType *vc=segue.destinationViewController;
        vc.txtFio = self.txtFio;
        vc.txtEmail = self.txtEmail;
        vc.txtPhone = self.txtPhone;
        vc.txtCity = self.txtCity;
        
        vc.deliveryType = @"pickup";
        
//        Pickup *vc=segue.destinationViewController;
//        vc.txtFio = self.txtFio;
//        vc.txtEmail = self.txtEmail;
//        vc.txtPhone = self.txtPhone;
//        vc.txtCity = self.txtCity;
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }
}
@end
