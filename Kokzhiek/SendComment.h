//
//  SendComment.h
//  Kokzhiek
//
//  Created by Admin on 04.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SendComment : UIViewController<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *txtText;
@property NSString *book_id;
@end
