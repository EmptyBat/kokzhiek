//
//  selected_change.m
//  Kokzhiek
//
//  Created by bugingroup on 23.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "selected_change.h"
#import "LGAlertView.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "MBProgressHUD.h"

@interface selected_change ()
{
    NSString*new_password;
    NSString*return_password;
    NSString*old_password;
}
@property (strong, nonatomic) LGAlertView *securityAlertView;
@end

@implementation selected_change

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.layoutMargins = UIEdgeInsetsZero;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0){
        static NSString *simpleTableIdentifier = @"change_about";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        return cell;
    }
    else{
        static NSString *simpleTableIdentifier = @"change_password";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        return cell;
        
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString*issocial=[[NSUserDefaults standardUserDefaults]objectForKey:@"issocial"];
    if([issocial isEqualToString:@"1"]){
        return 1;
    }
    return 2;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
-(void)textFieldDidChange :(UITextField *)theTextField{
    NSLog( @"text changed: %@", theTextField.text);
    if (theTextField.tag==2){
        return_password=theTextField.text;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag < 2)
        [_securityAlertView.textFieldsArray[(textField.tag + 1)] becomeFirstResponder];
    else
    {
        if ([_securityAlertView isButtonEnabledAtIndex:0])
            [_securityAlertView dismissAnimated:YES completionHandler:nil];
        else
            [textField resignFirstResponder];
    }
    
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag==0){
        old_password=textField.text;
    }
    else  if (textField.tag==1){
        new_password=textField.text;
    }
    else {
        return_password=textField.text;
    }
    
}
- (void)updateProgressWithAlertView:(LGAlertView *)alertView
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^(void)
                   {
                       if (alertView.progress >= 1.f)
                           [alertView dismissAnimated:YES completionHandler:nil];
                       else
                       {
                           float progress = alertView.progress+0.0025;
                           
                           if (progress > 1.f)
                               progress = 1.f;
                           
                           [alertView setProgress:progress progressLabelText:[NSString stringWithFormat:@"%.0f %%", progress*100]];
                           
                           [self updateProgressWithAlertView:alertView];
                       }
                   });
}
@end
