//
//  MyBooks.h
//  Kokzhiek
//
//  Created by bugingroup on 13.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface MyBooks :UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *menu;
@property(nonatomic)NSString*image_url;
@end
