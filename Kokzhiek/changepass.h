//
//  changepass.h
//  Kokzhiek
//
//  Created by Admin on 03.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface changepass : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtPass;
@property (weak, nonatomic) IBOutlet UITextField *txtPassNew;
@property (weak, nonatomic) IBOutlet UITextField *txtPassNewAgain;

@end
