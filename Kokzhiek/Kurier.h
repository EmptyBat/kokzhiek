//
//  Kurier.h
//  Kokzhiek
//
//  Created by Admin on 05.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Kurier : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtNaselPunkt;
@property (weak, nonatomic) IBOutlet UITextField *txtStreet;
@property (weak, nonatomic) IBOutlet UITextField *txtNDom;
@property (weak, nonatomic) IBOutlet UITextField *txtNKv;
@property (weak, nonatomic) IBOutlet UITextField *txtNPodiesd;

@property NSString *txtFio;
@property NSString *txtEmail;
@property NSString *txtPhone;
@property NSString *txtCity;
@end
