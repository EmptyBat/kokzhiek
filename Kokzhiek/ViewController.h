//
//  ViewController.h
//  Kokzhiek
//
//  Created by bugingroup on 03.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>
static NSString *const EPubViewControllerStoryboard = @"EPubViewControllerStoryboardId";
@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *menu;


@end

