
//
//  VkontakteDelegate.m
//  Kokzhiek
//
//  Created by Admin on 08.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//



#import "VkontakteDelegate.h"
#import "SendComment.h"
#import "Books_collection.h"
#import "AFNetworking.h"
#import <TLYShyNavBar/TLYShyNavBarManager.h>
#import "SWRevealViewController.h"
#import "SVProgressHUD.h"
#import "constants.h"
#import "ORGContainerCellView.h"
#import "ORGContainerCell.h"
#import "ORGArticleCollectionViewCell.h"
#import "BookDetail.h"
#import "EPubViewController.h"
#import "MBProgressHUD.h"
#import "FTWCache.h"
#import "NSString+MD5.h"


@implementation VkontakteDelegate

@synthesize username, realName, ID, photo, access_token, email, link;

+ (id)sharedInstance {
    static VkontakteDelegate *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[VkontakteDelegate alloc]init];
    });
    return __sharedInstance;
}
- (id) init {
    access_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"vk_token"];
    ID = [[NSUserDefaults standardUserDefaults] objectForKey:@"vk_id"];
    return  self;
}
-(void) loginWithParams:(NSMutableDictionary *)params {
    ID = [params objectForKey:@"user_id"];
    access_token = [params objectForKey:@"access_token"];
    //Сохраняемся, ребят!
    [[NSUserDefaults standardUserDefaults] setValue:access_token forKey:@"vk_token"];
    [[NSUserDefaults standardUserDefaults] setValue:ID forKey:@"vk_id"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //Теперь попробуем вытяныть некую информацию
    NSString *urlString = [NSString stringWithFormat:@"https://api.vk.com/method/users.get?uid=%@&access_token=%@", ID, access_token] ;
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSHTTPURLResponse *response = nil;
    NSError *error = nil;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    //NSData *objectData = [@"{\"2\":\"3\"}" dataUsingEncoding:NSUTF8StringEncoding];
   // NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
//    NSString *uid = [json objectForKey:@"uid"];
//    NSString *email = [json objectForKey:@"email"];
    
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //Тут произошла странная вещь - ответ должен вернуться в словаре, ан нет!
    //У меня не получилось пропарсить стандартными средствами.
    //Строим костыли, простите...
    NSArray* userData = [responseString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\":{},[]"]];
    realName = [userData objectAtIndex:14];
    realName = [realName stringByAppendingString:@" "];
    realName = [realName stringByAppendingString:[userData objectAtIndex:20]];
    //Кому надо, сохраняемся
    [[NSUserDefaults standardUserDefaults] setValue:@"vkontakte" forKey:@"SignedUpWith"];
    [[NSUserDefaults standardUserDefaults] setValue:realName forKey:@"RealUsername"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString*url2=[NSString stringWithFormat:@"%@/kk/mobile/socialcheck?provider_user_id=%@&provider=FacebookProvider",kBaseURL,ID];
    NSLog(@"%@",url2);
    [SVProgressHUD show];
    url2 = [url2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url2 parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //collection_data= [responseObject objectForKey:@"1"];
        [SVProgressHUD dismiss];
        
        if([[responseObject objectForKey:@"result"] isEqualToString:@"noemail"]){
        //http://kokzhiek.kz/kk/mobile/socialpastemail?provider_user_id=%@&provider=FacebookProvider&email=%@;
        }
        else{
            
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [SVProgressHUD dismiss];
    }];
    //http://kokzhiek.kz/kk/mobile/socialcheck?provider_user_id=" + loginResult.getAccessToken().getUserId() + "&provider=FacebookProvider
}
//-(void) postToWall {
//    //напишем что-нибудь на стене (вместо пробелов ставим "+")
//    NSString* message = @"vkontakte+wall+posting";
//    NSString *urlString = [NSString stringWithFormat:@"https://api.vk.com/method/wall.post?uid=%@&message=%@&attachments=http://google.com&access_token=%@", ID, message,access_token] ;
//    NSURL *url = [NSURL URLWithString:urlString];
//    //Теперь, если очень хочется, можно взглянуть на ответ
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    NSHTTPURLResponse *response = nil;
//    NSError *error = nil;
//    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
//}
@end