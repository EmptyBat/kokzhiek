//
//  Profile.m
//  Kokzhiek
//
//  Created by bugingroup on 22.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "Profile.h"
#import "SWRevealViewController.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "constants.h"
#import "FTWCache.h"
#import "NSString+MD5.h"
#import "MBProgressHUD.h"

@interface Profile ()
{
    NSArray*payment;
    NSArray*basket;
}
@end

@implementation Profile

- (void)viewDidLoad {
    [super viewDidLoad];
    _menu.target = self.revealViewController;
    _menu.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    self.navigationController.navigationBar.translucent = FALSE;
    
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.layoutMargins = UIEdgeInsetsZero;
    
    UIView*v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 130)];
    v.backgroundColor = [UIColor whiteColor];
    self.tableView.tableFooterView = v;
    self.btnFinishOrder.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated{
    self.btnFinishOrder.enabled = NO;
    self.btnFinishOrder.hidden = YES;
    self.vSum.hidden = YES;
    
    if (_segment.selectedSegmentIndex==1){
        
        NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
        NSString*password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
        NSString*url=[NSString stringWithFormat:@"%@kk/mobile/mybasket?email=%@&password=%@",kBaseURL,email,password];
        
        [SVProgressHUD show];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            NSLog(@"**+++%@",responseObject);
            [SVProgressHUD dismiss];
            basket = [responseObject objectForKey:@"books_in_basket"];
            
            if([basket isKindOfClass:[NSArray class]] && basket.count >0) {
                self.btnFinishOrder.enabled = YES;
                self.btnFinishOrder.hidden = NO;
                
                self.vSum.hidden = NO;
                self.lblSum.text = [NSString stringWithFormat:@"%@: %@ тг",NSLocalizedString(@"SUM", @""),[responseObject objectForKey:@"total_price"] ];
            }
            
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [SVProgressHUD dismiss];
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        }];
    }
    else if (_segment.selectedSegmentIndex==2){
        NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
        NSString*password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
        NSString*url=[NSString stringWithFormat:@"%@kk/mobile/orders?email=%@&password=%@",kBaseURL,email,password];
        
        [SVProgressHUD show];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            NSLog(@"**+++%@",responseObject);
            [SVProgressHUD dismiss];
            int value = [[responseObject objectForKey:@"result"] intValue];
            if (value !=0) {
                payment = [responseObject objectForKey:@"result"];
            }
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [SVProgressHUD dismiss];
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        }];
        
        
        [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    }else{
      [self.tableView reloadData];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)segment:(id)sender {
    if (_segment.selectedSegmentIndex==0){
        self.btnFinishOrder.hidden = YES;
        self.vSum.hidden = YES;
        [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    }
    else if (_segment.selectedSegmentIndex==1){
        self.btnFinishOrder.enabled = NO;
        self.btnFinishOrder.hidden = NO;
        self.btnFinishOrder.hidden = YES;
        self.vSum.hidden = YES;
        NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
        NSString*password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
        NSString*url=[NSString stringWithFormat:@"%@kk/mobile/mybasket?email=%@&password=%@",kBaseURL,email,password];
      
        [SVProgressHUD show];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            NSLog(@"**+++%@",responseObject);
            [SVProgressHUD dismiss];
            basket = [responseObject objectForKey:@"books_in_basket"];
            
            if([basket isKindOfClass:[NSArray class]] && basket.count >0) {
                self.btnFinishOrder.enabled = YES;
                self.btnFinishOrder.hidden = NO;
                
                self.vSum.hidden = NO;
                self.lblSum.text = [NSString stringWithFormat:@"%@: %@ тг",NSLocalizedString(@"SUM", @""),[responseObject objectForKey:@"total_price"] ];
            }
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [SVProgressHUD dismiss];
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        }];
    }
    else {
        self.btnFinishOrder.hidden = YES;
        self.vSum.hidden = YES;
        
        NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
        NSString*password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
        NSString*url=[NSString stringWithFormat:@"%@kk/mobile/orders?email=%@&password=%@",kBaseURL,email,password];
        
        [SVProgressHUD show];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            NSLog(@"**+++%@",responseObject);
            [SVProgressHUD dismiss];
            if ([[responseObject objectForKey:@"result"] isKindOfClass:[NSArray class]] != YES)
            {
                int value = [[responseObject objectForKey:@"result"] intValue];
                if (value !=0) {
                    payment = [responseObject objectForKey:@"result"];
                }
            }
            else {
                 payment = [responseObject objectForKey:@"result"];
            }
           
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [SVProgressHUD dismiss];
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        }];
        
        
        [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    }
    
    
}

- (IBAction)stepper:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    UILabel*stepper_value=(UILabel*)[cell viewWithTag:4];
    UIStepper*stepper=(UIStepper*)[cell viewWithTag:6];
    NSUInteger value = stepper.value;
    
    NSString *nv = [NSString stringWithFormat:@"%02lu", (unsigned long)value];
   // NSString *n1 = stepper_value.text;
    
    NSInteger v1 = value; //new
    NSInteger v2 = [stepper_value.text intValue]; //was
    
    if([stepper_value.text isEqualToString:nv] == NO){
        stepper_value.text = nv;
        
        
        NSDictionary *dictionary = [basket objectAtIndex:indexPath.row];
        
        NSString*frmt = [dictionary valueForKeyPath:@"pivot.format"];
        
        NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
        NSString*password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
        
        NSString*url=@"";
        NSString*book_id=[dictionary objectForKey:@"id"];
        
        if(v2 - v1 < 0){
            if([frmt isEqualToString:@"ebook"]){
                url=[NSString stringWithFormat:@"%@kk/mobile/mybasketquantityplus?book_id=%@&format=%@&email=%@&password=%@&operator=plus",kBaseURL,book_id,@"ebook",email,password];
            }else{
                url=[NSString stringWithFormat:@"%@kk/mobile/mybasketquantityplus?book_id=%@&format=%@&email=%@&password=%@&operator=plus",kBaseURL,book_id,@"paper",email,password];
            }
        }else{
            if([frmt isEqualToString:@"ebook"]){
                url=[NSString stringWithFormat:@"%@kk/mobile/mybasketquantityplus?book_id=%@&format=%@&email=%@&password=%@&operator=minus",kBaseURL,book_id,@"ebook",email,password];
            }else{
                url=[NSString stringWithFormat:@"%@kk/mobile/mybasketquantityplus?book_id=%@&format=%@&email=%@&password=%@&operator=minus",kBaseURL,book_id,@"paper",email,password];
            }
        }
    
        NSLog(@"%@",url);
        [SVProgressHUD show];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [SVProgressHUD dismiss];
            
            
            NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
            NSString*password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
            NSString*url=[NSString stringWithFormat:@"%@kk/mobile/mybasket?email=%@&password=%@",kBaseURL,email,password];
            
            [SVProgressHUD show];
            url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                NSLog(@"**+++%@",responseObject);
                [SVProgressHUD dismiss];
                basket = [responseObject objectForKey:@"books_in_basket"];
                [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
                
                if([basket isKindOfClass:[NSArray class]] && basket.count >0) {
                    self.btnFinishOrder.enabled = YES;
                    self.btnFinishOrder.hidden = NO;
                    
                    self.vSum.hidden = NO;
                    self.lblSum.text = [NSString stringWithFormat:@"%@: %@ тг",NSLocalizedString(@"SUM", @""),[responseObject objectForKey:@"total_price"] ];
                }
            } failure:^(NSURLSessionTask *operation, NSError *error) {
                NSLog(@"Error: %@", error);
                [SVProgressHUD dismiss];
                [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
            }];
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [SVProgressHUD dismiss];
        }];
    
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_segment.selectedSegmentIndex==0){
        if (indexPath.row==0){
            static NSString *simpleTableIdentifier = @"user";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            UIImageView*image_view=(UIImageView*)[cell viewWithTag:1];
            NSString *img = [[NSUserDefaults standardUserDefaults] objectForKey:@"avatar"];
            NSString *name_str = [[NSUserDefaults standardUserDefaults] objectForKey:@"name"];
            if([name_str isEqualToString:@""]){
                name_str = [[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
            }
            
            UILabel*name=(UILabel*)[cell viewWithTag:2];
            name.text=name_str;
            NSString*imagename=[NSString stringWithFormat:@"%@%@",kUser_avatar,img];
            NSLog(@"%@",imagename);
            NSURL *imageURL = [NSURL URLWithString:imagename];
            NSString *key = [imagename MD5Hash];
            NSData *data = [FTWCache objectForKey:key];
            if (data) {
                
                UIImage *image = [UIImage imageWithData:data];
                image_view.image = image;
                image_view.contentMode=UIViewContentModeScaleAspectFill;
                [image_view setClipsToBounds:YES];
            } else {
                image_view.image = [UIImage imageNamed:@"icon.png"];
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                dispatch_async(queue, ^{
                    NSData *data = [NSData dataWithContentsOfURL:imageURL];
                    [FTWCache setObject:data forKey:key];
                    UIImage *image = [UIImage imageWithData:data];
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        image_view.image = image;
                        image_view.contentMode=UIViewContentModeScaleAspectFill;
                        [image_view setClipsToBounds:YES];
                    });
                    
                });
            }
            cell.layoutMargins = UIEdgeInsetsZero;
            cell.preservesSuperviewLayoutMargins = false;
            return cell;
        }
        else if (indexPath.row==1){
            static NSString *simpleTableIdentifier = @"change";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            cell.layoutMargins = UIEdgeInsetsZero;
            cell.preservesSuperviewLayoutMargins = false;
            return cell;
            
        }
        else {
            static NSString *simpleTableIdentifier = @"social_networks";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            cell.layoutMargins = UIEdgeInsetsZero;
            cell.preservesSuperviewLayoutMargins = false;
            return cell;
            
        }
    }
    else if (_segment.selectedSegmentIndex==1){
        static NSString *simpleTableIdentifier = @"basket";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        UILabel*title=(UILabel*)[cell viewWithTag:1];
        UILabel*author=(UILabel*)[cell viewWithTag:2];
        UILabel*price=(UILabel*)[cell viewWithTag:3];
        UILabel*stepLabel=(UILabel*)[cell viewWithTag:4];
        UIImageView*image_view=(UIImageView*)[cell viewWithTag:5];
        UIStepper*stepp=(UIStepper*)[cell viewWithTag:6];
        NSDictionary *dictionary = [basket objectAtIndex:indexPath.row];
        
        NSString *title_str = [dictionary objectForKey:@"title"];
        NSString *image = [dictionary objectForKey:@"image"];
        
        NSString*frmt = [dictionary valueForKeyPath:@"pivot.format"];
        
        NSString *price_str = @"";
        NSString *f = @"";
        
        if([frmt isEqualToString:@"ebook"]){
            price_str = [dictionary objectForKey:@"ebook_price"];
            f = @"ebook";
//            stepp.backgroundColor = [UIColor lightGrayColor];
//            stepp.tintColor = [UIColor blackColor];
            stepp.alpha = 0.2;
            stepp.enabled = NO;
        }else{
            price_str = [dictionary objectForKey:@"price"];
            f = NSLocalizedString(@"PAPER", @"");
//            stepp.backgroundColor = [UIColor whiteColor];
//            stepp.tintColor = [UIColor blueColor];
            stepp.alpha = 1;
            stepp.enabled = YES;
        }
        price_str = [NSString stringWithFormat:@"%@ тг", price_str];
        
        NSString *frmtP = [NSString stringWithFormat:@"%@ %@: %@",price_str, @"Формат", f];
        
        NSMutableAttributedString *mutable = [[NSMutableAttributedString alloc] initWithString:frmtP];
        [mutable addAttribute: NSForegroundColorAttributeName value:[UIColor colorWithRed:81/255.f green:174/255.f blue:68/255.f alpha:255/255.f] range:[frmtP rangeOfString:price_str]];
        
        
        
        title.text= [NSString stringWithFormat:@"%@",title_str];
        author.text=[NSString stringWithFormat:@""];
        
        
        NSString*q = [dictionary valueForKeyPath:@"pivot.quantity"];
        NSUInteger value = [q intValue];
        
        stepp.value = value;
        
        NSString *qn = [NSString stringWithFormat:@"%02lu", (unsigned long)value];
        
        stepLabel.text=[NSString stringWithFormat:qn];
        [price setAttributedText:mutable];
        //price.text= frmtP;//[NSString stringWithFormat:@"%@",price_str];
        NSString*imagename=[NSString stringWithFormat:@"%@%@",KimageURL,image];
        imagename = [imagename stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *imageURL = [NSURL URLWithString:imagename];
        NSString *key = [imagename MD5Hash];
        NSData *data = [FTWCache objectForKey:key];
        if (data) {
            
            UIImage *image = [UIImage imageWithData:data];
            image_view.image = image;
            image_view.contentMode=UIViewContentModeScaleAspectFill;
            [image_view setClipsToBounds:YES];
            
            
        } else {
            image_view.image = [UIImage imageNamed:@"icon.png"];
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
            dispatch_async(queue, ^{
                NSData *data = [NSData dataWithContentsOfURL:imageURL];
                [FTWCache setObject:data forKey:key];
                UIImage *image = [UIImage imageWithData:data];
                dispatch_sync(dispatch_get_main_queue(), ^{
                    image_view.image = image;
                    image_view.contentMode=UIViewContentModeScaleAspectFill;
                    [image_view setClipsToBounds:YES];
                });
                
            });
        }
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        return cell;
    }
    else {
        NSDictionary *dictionary = [payment objectAtIndex:indexPath.row];
        
        static NSString *simpleTableIdentifier = @"responce";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        UILabel*lbl1=(UILabel*)[cell viewWithTag:1];
        
        NSString *status = [dictionary objectForKey:@"status"];
        if([status isEqualToString:@"process"]){
            status = NSLocalizedString(@"P_IN_PROCESS", @"");
        }else if([status isEqualToString:@"success"]){
            status = NSLocalizedString(@"P_SUCCESS", @"");
        }else if([status isEqualToString:@"fail"]){
            status = NSLocalizedString(@"P_FAILED", @"");
        }else if([status isEqualToString:@"delivered"]){
            status = NSLocalizedString(@"P_DELIVERED", @"");
        }else if([status isEqualToString:@"gift"]){
            status = NSLocalizedString(@"P_GIFTED", @"");
        }else{
            status = @"status";
        }
        
        
        lbl1.text = status;
        
        UILabel*lbl2=(UILabel*)[cell viewWithTag:2];
        lbl2.text = [NSString stringWithFormat:@"%@ тг",[dictionary objectForKey:@"order_amount"]];
        
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [formatter dateFromString:[dictionary objectForKey:@"created_at"]];
        
        NSLocale *locale = [NSLocale currentLocale];
        
        [formatter setLocale:locale];
        [formatter setDateFormat:@"d MMMM,\n yyyy"];
        
        UILabel*lbl3=(UILabel*)[cell viewWithTag:3];
        lbl3.text = [formatter stringFromDate:date];
        
        
        
        cell.layoutMargins = UIEdgeInsetsZero;
        cell.preservesSuperviewLayoutMargins = false;
        return cell;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_segment.selectedSegmentIndex==0){
        return 2;
    }
    else if (_segment.selectedSegmentIndex==1){
        if(![basket isKindOfClass:[NSArray class]]) {
            return 0;
        }
        return basket.count;
    }
    else {
        return payment.count;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_segment.selectedSegmentIndex==0){
        if (indexPath.row==0){
            return 77;
        }
        else if (indexPath.row==1){
            return 44;
        }
        else {
            return 0;
        }
        
    }
    else if (_segment.selectedSegmentIndex==1){
        return 186;
    }
    else {
        return 70;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (IBAction)logoutAction:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"email"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"password"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"name"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"address"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"avatar"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"phone"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"issocial"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie* cookie in
         [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
        [cookies deleteCookie:cookie];
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"Auth"];
    [[self navigationController] pushViewController:viewController animated:YES];
}

- (IBAction)removeItemAction:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    NSDictionary *dictionary = [basket objectAtIndex:indexPath.row];
    
    NSString*frmt = [dictionary valueForKeyPath:@"pivot.format"];
    
    NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    NSString*password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
    
    NSString*url=@"";
    NSString*book_id=[dictionary objectForKey:@"id"];
    
    if([frmt isEqualToString:@"ebook"]){
        url=[NSString stringWithFormat:@"%@kk/mobile/basket/delete?book_id=%@&format=%@&email=%@&password=%@",kBaseURL,book_id,@"ebook",email,password];
    }else{
        url=[NSString stringWithFormat:@"%@kk/mobile/basket/delete?book_id=%@&format=%@&email=%@&password=%@",kBaseURL,book_id,@"paper",email,password];
    }
    
    
    NSLog(@"%@",url);
    [SVProgressHUD show];
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        [SVProgressHUD dismiss];
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = NSLocalizedString(@"REMOVED_FROM_BAKSET", @"");
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        
        [hud hide:YES afterDelay:2];
        
        NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
        NSString*password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
        NSString*url=[NSString stringWithFormat:@"%@kk/mobile/mybasket?email=%@&password=%@",kBaseURL,email,password];
        //http://kokzhiek.local/kk/mobile/mybasket?email=sanzhar_nick@inbox.ru&password=maqsat
        [SVProgressHUD show];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            NSLog(@"**+++%@",responseObject);
            [SVProgressHUD dismiss];
            basket = [responseObject objectForKey:@"books_in_basket"];
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [SVProgressHUD dismiss];
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        }];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [SVProgressHUD dismiss];
    }];
}
@end
