//
//  SendComment.m
//  Kokzhiek
//
//  Created by Admin on 04.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "SendComment.h"
#import "Books_collection.h"
#import "AFNetworking.h"
#import <TLYShyNavBar/TLYShyNavBarManager.h>
#import "SWRevealViewController.h"
#import "SVProgressHUD.h"
#import "constants.h"
#import "ORGContainerCellView.h"
#import "ORGContainerCell.h"
#import "ORGArticleCollectionViewCell.h"
#import "BookDetail.h"
#import "EPubViewController.h"
#import "MBProgressHUD.h"
#import "FTWCache.h"
#import "NSString+MD5.h"

@interface SendComment (){
//    NSString *book_id;
}

@end

@implementation SendComment

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.txtText becomeFirstResponder];
    self.txtText.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)sendAction:(id)sender {
    [self.view endEditing:true];
    
    NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    NSString*password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
    
    if(![email isEqualToString:@""]){
        if (![self.txtText.text isEqualToString:@""] && ![self.txtText.text isEqualToString:@""]){
            
            
            NSString*url=[NSString stringWithFormat:@"http://kokzhiek.kz/kk/mobile/login?email=%@&password=%@",email,password];
            
            url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                
                NSString *user_id = [responseObject objectForKey:@"id"];
                
                
                NSString*url=[NSString stringWithFormat:@"%@kk/mobile/comment?book_id=%@&user_id=%@&comment=%@",kBaseURL,self->_book_id,user_id,self.txtText.text];
                NSLog(@"%@",url);
                [SVProgressHUD show];
                url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
                [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                    //collection_data= [responseObject objectForKey:@"1"];
                    [SVProgressHUD dismiss];
                    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                    hud.mode = MBProgressHUDModeText;
                    hud.labelText = NSLocalizedString(@"Спасибо", @"");
                    hud.margin = 10.f;
                    hud.yOffset = 150.f;
                    hud.removeFromSuperViewOnHide = YES;
                    self.txtText.text = @"";
                    [hud hide:YES afterDelay:2];
                } failure:^(NSURLSessionTask *operation, NSError *error) {
                    NSLog(@"Error: %@", error);
                    [SVProgressHUD dismiss];
                }];
            } failure:^(NSURLSessionTask *operation, NSError *error) {
                
            }];
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.labelText = NSLocalizedString(@"PLEASE_FILL_IN", @"");
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
        }
    }else{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = NSLocalizedString(@"NEED_AUTH", @"");
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

@end
