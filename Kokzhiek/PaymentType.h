//
//  PaymentType.h
//  Kokzhiek
//
//  Created by Admin on 05.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BMEpayViewController.h"

@interface PaymentType : UIViewController<UITableViewDataSource, UITableViewDelegate, BMEpayControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property NSString *txtFio;
@property NSString *txtEmail;
@property NSString *txtPhone;
@property NSString *txtCity;
@property NSString *deliveryType;

@property NSString *txtNaselPunkt;
@property NSString *txtStreet;
@property NSString *txtNDom;
@property NSString *txtNKv;
@property NSString *txtNPodiesd;



@property NSString *txtAdresat;
@property NSString *txtIndex;
//@property NSString *txtStreet;
//@property NSString *txtNDom;
//@property NSString *txtNKv;
//@property NSString *txtNaselPunkt;
@property NSString *txtDistrict;
@end
