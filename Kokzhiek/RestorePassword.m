//
//  RestorePassword.m
//  Kokzhiek
//
//  Created by Admin on 09.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "RestorePassword.h"
#import "Auth.h"
#import "SWRevealViewController.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "VkontakteDelegate.h"
#import "constants.h"
#import "SocialAuth.h"

@interface RestorePassword ()

@end

@implementation RestorePassword

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.txtEmail.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)restoreAction:(id)sender {
    
    if (![self.txtEmail.text isEqualToString:@""]&&![self.txtEmail.text isEqualToString:@""]){
        [self.view endEditing:true];
        NSString*url=@"http://kokzhiek.kz/password/reset";
        
        [SVProgressHUD show];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSURLRequest * urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest
                                              returningResponse:&response
                                                          error:&error];
        
        if (error == nil)
        {
            NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]; NSLog(@"%@", responseString);
            NSString *html = responseString;
            NSString *p1 = @"_token";
            
            
            html = [responseString stringByReplacingOccurrencesOfString: @" " withString:@""];
            
            NSRange i = NSMakeRange(0, [html rangeOfString:p1].location + p1.length-1);
            
            NSString *result = [html stringByReplacingCharactersInRange:i withString:@""];
            result = [result substringFromIndex:9];
            NSString *token = [result substringToIndex:40];
            
            
            NSString *myRequestString = [NSString stringWithFormat:@"email=%@&_token=%@",self.txtEmail.text, token];
            NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
            NSMutableURLRequest *request = [ [ NSMutableURLRequest alloc ] initWithURL: [ NSURL URLWithString:@"http://kokzhiek.kz/password/email"]];
            
            [request setHTTPMethod: @"POST"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
            [request setHTTPBody: myRequestData];
            
            response=nil;
            NSError *err;
            NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&err];
            NSString *content = [NSString stringWithUTF8String:[returnData bytes]];
            NSLog(@"responseData: %@", content);
            
            responseString = [[NSString alloc] initWithData:returnData encoding:NSNonLossyASCIIStringEncoding];
            
            if ([content rangeOfString:@"alert-success"].location != NSNotFound)
            {
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.labelText = NSLocalizedString(@"PASSWORD_SENT", @"");
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hide:YES afterDelay:2];
            }
        }
        
        [SVProgressHUD dismiss];
    }else{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = NSLocalizedString(@"PLEASE_FILL_IN", @"");
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    [theTextField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}

@end
