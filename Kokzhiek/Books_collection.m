//
//  Books_collection.m
//  Kokzhiek
//
//  Created by bugingroup on 14.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "Books_collection.h"
#import "AFNetworking.h"
#import <TLYShyNavBar/TLYShyNavBarManager.h>
#import "SWRevealViewController.h"
#import "SVProgressHUD.h"
#import "constants.h"
#import "ORGContainerCellView.h"
#import "ORGContainerCell.h"
#import "ORGArticleCollectionViewCell.h"
#import "BookDetail.h"
#import "EPubViewController.h"
#import "MBProgressHUD.h"
#import "FTWCache.h"
#import "NSString+MD5.h"
#import "CatalogTableViewCell.h"
@interface Books_collection ()
{
    NSArray*collection_data;
    NSDictionary*BookDetails;
}
@end

@implementation Books_collection

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.shyNavBarManager.scrollView = self.collection;
    //[self.shyNavBarManager setStickyExtensionView:NO];
    _menu.target = self.revealViewController;
    _menu.action = @selector(revealToggle:);
    [[UINavigationBar appearance] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    [self.searchBar setValue:NSLocalizedString(@"CANCEL", @"") forKey:@"_cancelButtonText"];
    self.searchBar.delegate = self;
    self.searchBar.placeholder = NSLocalizedString(@"BOOK_NAME", @"");
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self getData];
}
-(void)getData{
    NSString*url=[NSString stringWithFormat:@"%@mobile/all-books",kBaseURL];
    NSLog(@"%@",url);
    [SVProgressHUD show];
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        collection_data= responseObject;//[responseObject objectForKey:@"1"];
        NSLog(@"fafa %@",collection_data);
        [SVProgressHUD dismiss];
        [self.tableView setHidden:NO];
        [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [SVProgressHUD dismiss];
    }];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return collection_data.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return 168;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"catalog_cell";
    CatalogTableViewCell *cell = (CatalogTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CatalogTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
        NSDictionary *dictionary = [collection_data objectAtIndex:indexPath.row];
        
        
        UILabel *lbl = [cell.contentView viewWithTag:1];
        
        
        NSString* s = [dictionary objectForKey:@"sale"];
        long p = [[dictionary objectForKey:@"sale_percentage"] longValue];
        if(p == 0 || ![s isEqualToString:@"1"]){
            lbl.hidden = YES;
        }else{
            lbl.hidden = NO;
            lbl.text =  [NSString stringWithFormat:@"-%ld%%",p];
        }
        cell.title_lb.text = [dictionary objectForKey:@"title"];
        cell.desc_lb.text = [dictionary objectForKey:@"author"];
        cell.price_lb.text = [NSString stringWithFormat:@"%i",[[dictionary objectForKey:@"price"] intValue]];
        NSString *img = [dictionary objectForKey:@"image"];
        NSString*imagename=[NSString stringWithFormat:@"%@img/book_image/%@",kBaseURL,img];
        imagename = [imagename stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *imageURL = [NSURL URLWithString:imagename];
        NSString *key = [imagename MD5Hash];
        NSData *data = [FTWCache objectForKey:key];
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [indicator startAnimating];
        [indicator setCenter:cell.book_img.center];
        [cell.contentView addSubview:indicator];
        if (data) {
            
            UIImage *image = [UIImage imageWithData:data];
            cell.book_img.image = image;
            cell.book_img.contentMode=UIViewContentModeScaleAspectFill;
            [ cell.book_img setClipsToBounds:YES];
            [indicator removeFromSuperview];
            
        } else {
            cell.book_img.image = [UIImage imageNamed:@"icon.png"];
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
            dispatch_async(queue, ^{
                NSData *data = [NSData dataWithContentsOfURL:imageURL];
                [FTWCache setObject:data forKey:key];
                UIImage *image = [UIImage imageWithData:data];
                dispatch_sync(dispatch_get_main_queue(), ^{
                    cell.book_img.image = image;
                    cell.book_img.contentMode=UIViewContentModeScaleAspectFill;
                    [ cell.book_img setClipsToBounds:YES];
                    [indicator removeFromSuperview];
                });
                
            });
        
    
    }
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BookDetails = [collection_data objectAtIndex:indexPath.row];
    NSLog(@"affafafa   %@",BookDetails);
    [self performSegueWithIdentifier:@"BookDetail" sender:indexPath];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"BookDetail"]) {
        BookDetail*Detail=segue.destinationViewController;
        Detail.image_url = [BookDetails objectForKey:@"image"];
        Detail.book_title = [BookDetails objectForKey:@"title"];
        Detail.author = [BookDetails objectForKey:@"author"];
        Detail.isbn = [BookDetails objectForKey:@"isbn"];
        Detail.description_book = [BookDetails objectForKey:@"description"];
        Detail.ebook_price = [BookDetails objectForKey:@"ebook_price"];
        Detail.price = [BookDetails objectForKey:@"price"];
        Detail.page = [BookDetails objectForKey:@"page"];
        Detail.publish_year = [BookDetails objectForKey:@"publish_year"];
        Detail.formats = [BookDetails objectForKey:@"formats"];
        Detail.book_id = [BookDetails objectForKey:@"id"];
        Detail.status = [BookDetails objectForKey:@"status"];
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
    }
}

- (IBAction)change_category:(id)sender {
    NSArray*books=@[@"Құран және Сүннет",@"Иман",@"Құлшылық",@"Тұлға",@"Көркем мінез",@"Әр түрлі"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        UIAlertView *complexAlert = [[UIAlertView alloc] initWithTitle:@"Выберите категорию"
                                                               message:@"" delegate:self  cancelButtonTitle:@"Отмена"otherButtonTitles:nil ,nil];
        for(NSString *buttonTitle in books) {
            [complexAlert addButtonWithTitle:buttonTitle];
        }
        complexAlert.tag=1;
        [complexAlert show];
        
    }
    else{
        UIActionSheet *select = [[UIActionSheet alloc] initWithTitle:@"Выберите категорию" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil, nil];
        select.tag = 1;
        for (NSString *title in books) {
            [select addButtonWithTitle:title];
        }
        select.tintColor=[UIColor blackColor];
        select.cancelButtonIndex = [select addButtonWithTitle:@"Отмена"];
        [select showInView:self.view];
        
    }
}

-(void)category:(NSString*)str{
    NSString*url=[NSString stringWithFormat:@"%@ru/mobile/category-books",kBaseURL];
    NSLog(@"%@",url);
    [SVProgressHUD show];
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        collection_data= [responseObject objectForKey:str];
        [SVProgressHUD dismiss];
        [self.tableView setHidden:NO];
        [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [SVProgressHUD dismiss];
    }];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    if (buttonIndex == [alertView cancelButtonIndex]) {
    }
    else {
        [self category:[NSString stringWithFormat:@"%ld",buttonIndex+1]];
    }
    
}

- ( void ) actionSheet : ( UIActionSheet * ) actionSheet clickedButtonAtIndex : ( NSInteger ) buttonIndex {
    if (buttonIndex == [actionSheet cancelButtonIndex]) {
    }
    else {
        [self category:[NSString stringWithFormat:@"%ld",buttonIndex+1]];
    }
}


- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    // called only once
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    // called twice every time
    [searchBar setShowsCancelButton:YES animated:YES];
//    for (UIView *subView in searchBar.subviews){
//        if([subView isKindOfClass:[UIButton class]]){
//            [(UIButton*)subView setTitle:NSLocalizedString(@"CANCEL", @"") forState:UIControlStateNormal];
//        }
//    }
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    // called only once
    [searchBar setShowsCancelButton:NO animated:YES];
    
    if([self.searchBar.text isEqualToString:@""]){
        NSString*url=[NSString stringWithFormat:@"%@ru/mobile/category-books",kBaseURL];
        NSLog(@"%@",url);
        [SVProgressHUD show];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            collection_data= [responseObject objectForKey:@"1"];
            [SVProgressHUD dismiss];
            [self.tableView setHidden:NO];
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [SVProgressHUD dismiss];
        }];
    }
    
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:true];
    
    if([self.searchBar.text isEqualToString:@""]){
        NSString*url=[NSString stringWithFormat:@"%@ru/mobile/category-books",kBaseURL];
        NSLog(@"%@",url);
        [SVProgressHUD show];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            collection_data= [responseObject objectForKey:@"1"];
            [SVProgressHUD dismiss];
            [self.tableView setHidden:NO];
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [SVProgressHUD dismiss];
        }];
    }else{
        NSString*url=[NSString stringWithFormat:@"%@kk/mobile/search?query=%@",kBaseURL, self.searchBar.text];
        NSLog(@"%@",url);
        [SVProgressHUD show];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            collection_data= [responseObject objectForKey:@"books"];
            if (collection_data.count == 0){
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.labelText = NSLocalizedString(@"NO_FINDED", @"");
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;

                [hud hide:YES afterDelay:3];
            }
            [SVProgressHUD dismiss];
            [self.tableView setHidden:NO];
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [SVProgressHUD dismiss];
        }];
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    // called only once
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [self.view endEditing:true];
}
@end
