//
//  Callback.m
//  Kokzhiek
//
//  Created by Admin on 14.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "Callback.h"
#import "Books_collection.h"
#import "AFNetworking.h"
#import <TLYShyNavBar/TLYShyNavBarManager.h>
#import "SWRevealViewController.h"
#import "SVProgressHUD.h"
#import "constants.h"
#import "ORGContainerCellView.h"
#import "ORGContainerCell.h"
#import "ORGArticleCollectionViewCell.h"
#import "BookDetail.h"
#import "EPubViewController.h"
#import "MBProgressHUD.h"
#import "FTWCache.h"
#import "NSString+MD5.h"

@interface Callback ()

@end

@implementation Callback

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"CALLBACK", @"");
    
    self.txtFio.delegate=self;
    self.txtEmail.delegate=self;
    self.txtText.delegate=self;
    
    self.txtFio.placeholder = NSLocalizedString(@"FIO", @"");
    self.lblForText.text = NSLocalizedString(@"TEXT", @"");
    NSString*name=[[NSUserDefaults standardUserDefaults]objectForKey:@"name"];
    NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    
    self.txtFio.text=name;
    self.txtEmail.text=email;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)sendAction:(id)sender {
    [self.view endEditing:YES];
    
    if (![self.txtFio.text isEqualToString:@""] && ![self.txtFio.text isEqualToString:@""]
        && ![self.txtEmail.text isEqualToString:@""] && ![self.txtEmail.text isEqualToString:@""]
        && ![self.txtText.text isEqualToString:@""] && ![self.txtText.text isEqualToString:@""]){
        //    NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
        //    NSString*password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
        NSString*url=[NSString stringWithFormat:@"%@kk/mobile/feedback?name=%@&email=%@&text=%@",kBaseURL,self.txtFio.text,self.txtEmail.text,self.txtText.text];
        NSLog(@"%@",url);
        [SVProgressHUD show];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            //collection_data= [responseObject objectForKey:@"1"];
            [SVProgressHUD dismiss];
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.labelText = NSLocalizedString(@"SENT", @"");
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.removeFromSuperViewOnHide = YES;
            
            [hud hide:YES afterDelay:2];
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [SVProgressHUD dismiss];
        }];
    }else{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = NSLocalizedString(@"PLEASE_FILL_IN", @"");
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField==self.txtFio){
        [self.txtEmail becomeFirstResponder];
    }
    else   if (theTextField==self.txtEmail){
        [self.txtText becomeFirstResponder];
    }
    else  {
        [theTextField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}

@end
