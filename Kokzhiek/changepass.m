//
//  changepass.m
//  Kokzhiek
//
//  Created by Admin on 03.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "changepass.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"

@interface changepass ()

@end

@implementation changepass

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _txtPass.delegate=self;
    _txtPassNew.delegate=self;
    _txtPassNewAgain.delegate=self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField==self.txtPass){
        [self.txtPassNew becomeFirstResponder];
    }
    else   if (theTextField==self.txtPassNew){
        [self.txtPassNewAgain becomeFirstResponder];
    }
    else  {
        [theTextField resignFirstResponder];
    }
    return YES;
}
- (IBAction)saveAction:(id)sender {
    //    @"Ескі құпия сөз";
    //    @"Жаңа құпия сөз";
    //    @"Жаңа құпия сөз";
    [self.view endEditing:true];
    
    NSString *passOld = _txtPass.text;
    NSString *passNew = _txtPassNew.text;
    NSString *passAgain = _txtPassNewAgain.text;
    
    if (![passOld isEqualToString:@""]&&![passNew isEqualToString:@""] &&![passAgain isEqualToString:@""]){
        if ([passNew isEqualToString:passAgain]){
            NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
            NSString*url=[NSString stringWithFormat:@"http://kokzhiek.kz/kk/mobile/password/edit?email=%@&old_password=%@&password=%@&re_password=%@",email,passOld,passNew,passAgain];
            
            [SVProgressHUD show];
            url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                NSLog(@"**+++%@",responseObject);
                [SVProgressHUD dismiss];
                if ([[responseObject objectForKey:@"result" ] isKindOfClass:[NSString class]]){
                    if ([[responseObject objectForKey:@"result" ]isEqualToString:@"success"]) {
                        [[NSUserDefaults standardUserDefaults] setObject:passNew forKey:@"password"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                        hud.mode = MBProgressHUDModeText;
                        hud.labelText = NSLocalizedString(@"SAVED", @"");
                        hud.margin = 10.f;
                        hud.yOffset = 150.f;
                        hud.removeFromSuperViewOnHide = YES;
                        
                        [hud hide:YES afterDelay:2];
                        
                    }
                    
                }
                else {
                    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                    hud.mode = MBProgressHUDModeText;
                    hud.labelText = NSLocalizedString(@"WRONG_DATA", @"");
                    hud.margin = 10.f;
                    hud.yOffset = 150.f;
                    hud.removeFromSuperViewOnHide = YES;
                    [hud hide:YES afterDelay:2];
                    
                    
                }
            } failure:^(NSURLSessionTask *operation, NSError *error) {
                NSLog(@"Error: %@", error);
                [SVProgressHUD dismiss];
            }];
            
        }
        else {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.labelText =NSLocalizedString(@"PASSWORDS_DIFFER", @"");
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.removeFromSuperViewOnHide = YES;
            
            [hud hide:YES afterDelay:2];
            
        }
    }
    else {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = NSLocalizedString(@"PLEASE_FILL_IN", @"");
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        
        [hud hide:YES afterDelay:2];
        
    }
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}
@end
