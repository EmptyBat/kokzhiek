//
//  Library-5.m
//  Kokzhiek
//
//  Created by bugingroup on 07.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "Library-5.h"
#import "AFNetworking.h"
#import <TLYShyNavBar/TLYShyNavBarManager.h>
#import "SWRevealViewController.h"
#import "SVProgressHUD.h"
#import "constants.h"
#import "FTWCache.h"
#import "NSString+MD5.h"
#import "CollectionViewCell.h"
@interface Library_5 ()
{
    NSArray*book;
}
@end

@implementation Library_5
static NSString *CellIdentifier = @"cell";
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:CellIdentifier];
    self.collectionView.backgroundColor=[UIColor whiteColor];
    [self getData];
}
-(void)getData{
    NSString*url=[NSString stringWithFormat:@"%@ru/mobile/category-books",kBaseURL];
    NSLog(@"%@",url);
    [SVProgressHUD show];
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        book= [responseObject objectForKey:@"5"];
        [SVProgressHUD dismiss];
        [self.collectionView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [SVProgressHUD dismiss];
    }];
    
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return book.count;
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
        CollectionViewCell *cell = (CollectionViewCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"cell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    NSDictionary *dictionary = [book objectAtIndex:indexPath.row];
    NSString *img = [dictionary objectForKey:@"image"];
    NSString*imagename=[NSString stringWithFormat:@"%@img/book_image/%@",kBaseURL,img];
    NSURL *imageURL = [NSURL URLWithString:imagename];
    NSString *key = [imagename MD5Hash];
    NSData *data = [FTWCache objectForKey:key];
    if (data) {
        UIImage *image = [UIImage imageWithData:data];
        cell.image.image = image;
        cell.image.contentMode=UIViewContentModeScaleAspectFill;
        [ cell.image setClipsToBounds:YES];
    } else {
        cell.image.image = [UIImage imageNamed:@"icon.png"];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            NSData *data = [NSData dataWithContentsOfURL:imageURL];
            [FTWCache setObject:data forKey:key];
            UIImage *image = [UIImage imageWithData:data];
            dispatch_sync(dispatch_get_main_queue(), ^{
                cell.image.image = image;
                cell.image.contentMode=UIViewContentModeScaleAspectFill;
                [ cell.image setClipsToBounds:YES];
            });
            
        });
    }
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *currentSection = [book objectAtIndex:indexPath.row];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"BookDetail"
     object:currentSection];
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize cellSize;
    
    cellSize.width = 142;
    
    cellSize.height = 210;
    
    return cellSize;
}


-(NSString *)nameForSwipeContainer:(XLSwipeContainerController *)swipeContainer
{
    return @"Көркем мінез";
}

-(UIColor *)colorForSwipeContainer:(XLSwipeContainerController *)swipeContainer
{
    return [UIColor whiteColor];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
