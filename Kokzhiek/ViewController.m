//
//  ViewController.m
//  Kokzhiek
//
//  Created by bugingroup on 03.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"
#import <TLYShyNavBar/TLYShyNavBarManager.h>
#import "SWRevealViewController.h"
#import "SVProgressHUD.h"
#import "constants.h"
#import "ORGContainerCellView.h"
#import "ORGContainerCell.h"
#import "ORGArticleCollectionViewCell.h"
#import "BookDetail.h"
#import "EPubViewController.h"
#import "MBProgressHUD.h"

@interface ViewController ()<MBProgressHUDDelegate> {
    MBProgressHUD *HUD;
    NSArray*book1;
    NSArray*book2;
    NSArray*book3;
    NSDictionary*BookDetails;
    int return_count;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(BookDetail:)
                                                 name:@"BookDetail" object:nil];
 //   self.shyNavBarManager.scrollView = self.tableView;
   // [self.shyNavBarManager setStickyExtensionView:NO];
    [self.tableView registerClass:[ORGContainerCell class] forCellReuseIdentifier:@"ORGContainerCell"];
    _menu.target = self.revealViewController;
    _menu.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self getData];
    self.automaticallyAdjustsScrollViewInsets = NO;
}



- (void)myProgressTask {
    float progress = 0.0f;
    while (progress < 1.0f) {
        progress += 0.01f;
        HUD.progress = progress;
        usleep(50000);
    }
}

- (void)BookDetail:(NSNotification *)notice{
    BookDetails = [notice object];
    [self performSegueWithIdentifier:@"BookDetail" sender:self];
}

-(void)getData{
    NSString*url=[NSString stringWithFormat:@"%@ru/mobile/category-books",kBaseURL];
    NSLog(@"%@",url);
    [SVProgressHUD show];
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        book1= [responseObject objectForKey:@"7"];
        book2= [responseObject objectForKey:@"8"];
        book3= [responseObject objectForKey:@"9"];
        if (book3.count == 0) {
            return_count = 2;
        }
        
        book1 = [[book1 reverseObjectEnumerator] allObjects];
        book2 = [[book2 reverseObjectEnumerator] allObjects];
        book3 = [[book3 reverseObjectEnumerator] allObjects];
        
        [SVProgressHUD dismiss];
        [self.tableView setHidden:NO];
        [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [SVProgressHUD dismiss];
    }];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return return_count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(13, 0, 200, 30)];
    [label setFont:[UIFont boldSystemFontOfSize:17]];
    
    UIButton *more = [[UIButton alloc] initWithFrame:CGRectMake(tableView.frame.size.width-60, 0, 60, 30)];
    [more setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [more setTitle:NSLocalizedString(@"MORE", "") forState:UIControlStateNormal];
    more.hidden = YES;
    more.titleLabel.textAlignment = NSTextAlignmentRight;
    [more.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];

    
    if (section == 0){
        [label setText:NSLocalizedString(@"BOOKS_NEW","")];
    }
    else if (section == 1){
        [label setText:NSLocalizedString(@"BOOKS_POPULAR","")];
    }
    else{
        [label setText:NSLocalizedString(@"BOOKS_RECENT","")];
    }
    
    [view addSubview:label];
    [view addSubview:more];
    [view setBackgroundColor:[UIColor whiteColor]];
    
    return view;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0){
        ORGContainerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ORGContainerCell"];
        [cell setCollectionData:book1];
        cell.userInteractionEnabled=YES;
        return cell;
    }
    else if (indexPath.section==1){
        ORGContainerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ORGContainerCell"];
        [cell setCollectionData:book2];
        cell.userInteractionEnabled=YES;
        return cell;
        
    }
    else {
        ORGContainerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ORGContainerCell"];
        [cell setCollectionData:book3];
        cell.userInteractionEnabled=YES;
        return cell;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"BookDetail"]) {
        BookDetail*Detail=segue.destinationViewController;
        Detail.image_url = [BookDetails objectForKey:@"image"];
        Detail.book_title = [BookDetails objectForKey:@"title"];
        Detail.author = [BookDetails objectForKey:@"author"];
        Detail.isbn = [BookDetails objectForKey:@"isbn"];
        Detail.description_book = [BookDetails objectForKey:@"description"];
        Detail.ebook_price = [BookDetails objectForKey:@"ebook_price"];
        Detail.price = [BookDetails objectForKey:@"price"];
        Detail.page = [BookDetails objectForKey:@"page"];
        Detail.publish_year = [BookDetails objectForKey:@"publish_year"];
        Detail.formats = [BookDetails objectForKey:@"formats"];
        Detail.book_id = [BookDetails objectForKey:@"id"];
        Detail.status = [BookDetails objectForKey:@"status"];
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
