//
//  Share.m
//  Kokzhiek
//
//  Created by Admin on 17.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "Share.h"
#import "SWRevealViewController.h"
#import <MessageUI/MFMessageComposeViewController.h>
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>

@interface Share ()

@end

@implementation Share

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"MENU_SHARE","");
    
    _menu.target = self.revealViewController;
    _menu.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    UIImage *img = [UIImage imageNamed:@"fon"];
    UIImageView *imgV = [[UIImageView alloc] initWithImage:img];
    imgV.contentMode = UIViewContentModeScaleAspectFill;
    self.tableView.backgroundView = imgV;
    
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.layoutMargins = UIEdgeInsetsZero;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.cell1.layoutMargins = UIEdgeInsetsZero;
    self.cell1.preservesSuperviewLayoutMargins = false;
    self.cell2.layoutMargins = UIEdgeInsetsZero;
    self.cell2.preservesSuperviewLayoutMargins = false;
    self.cell3.layoutMargins = UIEdgeInsetsZero;
    self.cell3.preservesSuperviewLayoutMargins = false;
    self.cell4.layoutMargins = UIEdgeInsetsZero;
    self.cell4.preservesSuperviewLayoutMargins = false;
    self.cell5.layoutMargins = UIEdgeInsetsZero;
    self.cell5.preservesSuperviewLayoutMargins = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
   
    if(indexPath.row == 0){
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
            SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            
            [controller setInitialText:@"Скачай приложение Көкжиек: https://appsto.re/kz/Jz-Fdb.i"];
            [self presentViewController:controller animated:YES completion:Nil];
        }else{
            NSString* authLink = [NSString stringWithFormat:@"http://www.facebook.com/sharer.php?u=%@&t=%@",
                                  @"https://appsto.re/kz/Jz-Fdb.i",
                                  @"Скачай приложение Көкжиек"];
            
            authLink = [authLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:authLink]];
            //facebook: function (purl, ptitle, pimg, text) {
            //    url = 'http://www.facebook.com/sharer.php?u=' + purl + '&t=' + ptitle + '&src=sp&ve=2';
            //    url += '&p[title]=' + (ptitle);
            //    url += '&p[summary]=' + (text);
            //    url += '&p[url]=' + (purl);
            //    url += '&p[images][0]=' + (pimg);
            //
            //    url = 'https://www.facebook.com/sharer/sharer.php?u=' + purl;
            //    
            //    Share.popup(url);
            //},
        }
    }else if(indexPath.row == 1){
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            SLComposeViewController *tweetSheet = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeTwitter];
            [tweetSheet setInitialText:@"Скачай приложение Көкжиек: https://appsto.re/kz/Jz-Fdb.i"];
            [self presentViewController:tweetSheet animated:YES completion:nil];
        }else{
            NSString* authLink = [NSString stringWithFormat:@"http://twitter.com/share?url=%@&text=%@",
                                  @"https://appsto.re/kz/Jz-Fdb.i",
                                  @"Скачай приложение Көкжиек"];
            authLink = [authLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:authLink]];
            //twitter: function (purl, ptitle) {
            //    url = 'http://twitter.com/share?';
            //    url += 'text=' + (ptitle);
            //    url += '&url=' + (purl);
            //    url += '&counturl=' + (purl);
        }
    }else if(indexPath.row == 2){
        NSString* authLink = [NSString stringWithFormat:@"http://vkontakte.ru/share.php?url=%@&description=%@",
                              @"https://appsto.re/kz/Jz-Fdb.i",
                              @"Скачай приложение Көкжиек"];
        authLink = [authLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        //    url = 'http://vkontakte.ru/share.php?';
        //    url += 'url=' + (purl);
        //    url += '&title=' + (ptitle);
        //    url += '&description=' + (text);
        //    url += '&image=' + (pimg);
        //    url += '&noparse=true';
        
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:authLink]];
    }else if(indexPath.row == 3){
        if ([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
            [mailController setMailComposeDelegate:self];
            [mailController setSubject:@"Приложение Көкжиек"];
            //[mailController setToRecipients:@[@"email1", @"email2"];
            [mailController setMessageBody:@"Скачай приложение Көкжиек: https://appsto.re/kz/Jz-Fdb.i" isHTML:NO];
            [self presentViewController:mailController animated:YES completion:nil];
        }
    }else if(indexPath.row == 4){
        MFMessageComposeViewController* composeVC = [[MFMessageComposeViewController alloc] init];
        composeVC.messageComposeDelegate = self;
        
        // Configure the fields of the interface.
        composeVC.recipients = @[];
        composeVC.body = @"Скачай приложение Көкжиек: https://appsto.re/kz/Jz-Fdb.i";
        
        // Present the view controller modally.
        [self presentViewController:composeVC animated:YES completion:nil];
    }
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)aboutAction:(id)sender {
}

@end