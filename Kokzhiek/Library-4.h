//
//  Library-4.h
//  Kokzhiek
//
//  Created by bugingroup on 07.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLSwipeContainerController.h"
@interface Library_4 : UICollectionViewController<XLSwipeContainerChildItem,UICollectionViewDataSource,UICollectionViewDelegate>

@end
