//
//  CollectionViewCell.h
//  Kokzhiek
//
//  Created by bugingroup on 07.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet UIImageView *image;
@end
