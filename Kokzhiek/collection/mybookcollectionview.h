//
//  mybookcollectionview.h
//  Kokzhiek
//
//  Created by Almas Abdrasilov on 04.08.2018.
//  Copyright © 2018 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mybookcollectionview : UIView
@property (weak, nonatomic) IBOutlet UIButton *btnL;
@property (weak, nonatomic) IBOutlet UIButton *btnR;
- (void)setCollectionData:(NSArray *)collectionData;
@end
@interface UIImage (PhoenixMaster)
- (UIImage *) makeThumbnailOfSize:(CGSize)size;
@end
