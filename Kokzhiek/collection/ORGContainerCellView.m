//
//  ORGContainerCellView.m
//  HorizontalCollectionViews
//
//  Created by James Clark on 4/22/13.
//  Copyright (c) 2013 OrgSync, LLC. All rights reserved.
//

#import "ORGContainerCellView.h"
#import "ORGArticleCollectionViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "FTWCache.h"
#import "NSString+MD5.h"
#import "Location.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "constants.h"

@interface ORGContainerCellView () <UICollectionViewDataSource, UICollectionViewDelegate>
{
    NSArray*_collectionData;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation ORGContainerCellView

- (void)awakeFromNib {
    
    
    // Register the colleciton cell
    [_collectionView registerNib:[UINib nibWithNibName:@"ORGArticleCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ORGArticleCollectionViewCell"];
}
#pragma mark - Getter/Setter overrides
- (void)setCollectionData:(NSArray *)collectionData {
    _collectionData=collectionData;
    [_collectionView setContentOffset:CGPointZero animated:NO];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
}



- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
#pragma mark - UICollectionViewDataSource methods
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _collectionData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    ORGArticleCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ORGArticleCollectionViewCell" forIndexPath:indexPath];
    NSDictionary *dictionary = [_collectionData objectAtIndex:indexPath.row];
    NSString *img = [dictionary objectForKey:@"image"];
    NSString*imagename=[NSString stringWithFormat:@"%@img/book_image/%@",kBaseURL,img];
    imagename = [imagename stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *imageURL = [NSURL URLWithString:imagename];
    NSString *key = [imagename MD5Hash];
    NSData *data = [FTWCache objectForKey:key];
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [indicator startAnimating];
    [indicator setCenter:cell.articleImage.center];
    [cell.contentView addSubview:indicator];
    cell.title_lb.text = [dictionary objectForKey:@"title"];
    cell.subtitle_lb.text = [dictionary objectForKey:@"description"];
    int view_count_paper = [[dictionary objectForKey:@"view_count_paper"] intValue];
    cell.count_lb.text = [NSString stringWithFormat:@"%i",view_count_paper];
    NSLog(@"");
    if (data) {
        UIImage *image = [UIImage imageWithData:data];
        cell.articleImage.image = image;
        cell.articleImage.contentMode=UIViewContentModeScaleAspectFill;
        [ cell.articleImage setClipsToBounds:YES];
        [indicator removeFromSuperview];
        
    } else {
        cell.articleImage.image = [UIImage imageNamed:@"icon.png"];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            NSData *data = [NSData dataWithContentsOfURL:imageURL];
            [FTWCache setObject:data forKey:key];
            UIImage *image = [UIImage imageWithData:data];
            dispatch_sync(dispatch_get_main_queue(), ^{
                cell.articleImage.image = image;
                cell.articleImage.contentMode=UIViewContentModeScaleAspectFill;
                [ cell.articleImage setClipsToBounds:YES];
                [indicator removeFromSuperview];
            });
            
        });
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *currentSection = [_collectionData objectAtIndex:indexPath.row];
    NSString *source = [currentSection objectForKey:@"source"];
    if (source==NULL){
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"BookDetail"
         object:currentSection];
    }
    else {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"BookDetail"
         object:currentSection];
        return;
        
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"MyBooks"
         object:currentSection];
        //ORGArticleCollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
        
        UICollectionViewLayoutAttributes *attributes = [collectionView layoutAttributesForItemAtIndexPath:indexPath];
        
        CGRect cellRect = attributes.frame;
        
        CGRect cellFrameInSuperview = [collectionView convertRect:cellRect toView:[collectionView superview]];
        
        
        [[NSUserDefaults standardUserDefaults] setFloat:cellFrameInSuperview.origin.x forKey:@"book_x"];
        [[NSUserDefaults standardUserDefaults] setFloat:cellFrameInSuperview.origin.y+40 forKey:@"book_y"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}
- (NSString *)documentsPathForFileName:(NSString *)name {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}
- (IBAction)goLeftAction:(id)sender {
    
    CGFloat x = self.collectionView.contentOffset.x - 100;
    if(x <0){
        x = 0;
    }
    
    self.collectionView.contentOffset = CGPointMake(x, 0);
}
- (IBAction)goRightAction:(id)sender {
    CGFloat x = self.collectionView.contentOffset.x + 100;
    
    
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat xx = self.collectionView.contentSize.width - screenWidth + 100;
    
    if(x > xx){
        x = xx;
    }
    self.collectionView.contentOffset = CGPointMake(x, 0);
}

@end
@implementation UIImage (PhoenixMaster)
- (UIImage *) makeThumbnailOfSize:(CGSize)size
{
    UIGraphicsBeginImageContextWithOptions(size, NO, UIScreen.mainScreen.scale);
    [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newThumbnail = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    if(newThumbnail == nil)
        NSLog(@"could not scale image");
    return newThumbnail;
}
@end
