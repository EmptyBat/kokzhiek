//
//  ORGArticleCollectionViewCell.h
//  HorizontalCollectionViews
//
//  Created by James Clark on 4/23/13.
//  Copyright (c) 2013 OrgSync, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ORGArticleCollectionViewCell : UICollectionViewCell
@property (weak) IBOutlet UIImageView *articleImage;
@property (weak, nonatomic) IBOutlet UILabel *count_lb;
@property (weak, nonatomic) IBOutlet UILabel *title_lb;
@property (weak, nonatomic) IBOutlet UILabel *subtitle_lb;
@end
