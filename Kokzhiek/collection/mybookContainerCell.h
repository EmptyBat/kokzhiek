//
//  mybookContainerCell.h
//  Kokzhiek
//
//  Created by Almas Abdrasilov on 04.08.2018.
//  Copyright © 2018 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mybookContainerCell : UITableViewCell
- (void)setCollectionData:(NSArray *)collectionData;
@end
