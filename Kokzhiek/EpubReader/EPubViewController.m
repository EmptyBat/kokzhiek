//
//  DetailViewController.m
//  AePubReader
//
//  Created by Federico Frappi on 04/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EPubViewController.h"
#import "ChapterListViewController.h"
#import "SearchResultsViewController.h"
#import "SearchResult.h"
#import "UIWebView+SearchWebView.h"
#import "Chapter.h"
#define DURING_TIME         0.5

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
@interface EPubViewController()
{
    CGPoint _bookViewOrignCenter;
    
    UIModalTransitionStyle _modalTransitionStyle;
    UIView *v;
    NSString* bgColor;
    NSString* txtColor;
    NSString* font;
    NSInteger ii;
    NSInteger jj;
}

- (void) gotoNextSpine;
- (void) gotoPrevSpine;
- (void) gotoNextPage;
- (void) gotoPrevPage;

- (int) getGlobalPageCount;

- (void) gotoPageInCurrentSpine: (int)pageIndex;
- (void) updatePagination;

- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex;


@end

@implementation EPubViewController

@synthesize loadedEpub, toolbar, webView;
@synthesize chapterListButton, decTextSizeButton, incTextSizeButton;
@synthesize currentPageLabel, pageSlider, searching;
@synthesize currentSearchResult;

#pragma mark -

- (void) loadEpub:(NSURL*) epubURL{
    
    currentSpineIndex = 0;
    currentPageInSpineIndex = 0;
    pagesInCurrentSpineCount = 0;
    totalPagesCount = 0;
    searching = NO;
    epubLoaded = NO;
    self.loadedEpub = [[EPub alloc] initWithEPubPath:[epubURL path]];
    epubLoaded = YES;
    NSLog(@"loadEpub");
    [self updatePagination];
    
    
    self->v = [self.view viewWithTag:66];
    self->v.hidden = YES;
    self->v.layer.borderColor = [UIColor grayColor].CGColor;
    self->v.layer.cornerRadius = 5;
    self->v.layer.borderWidth = 1;
    
    UIButton *b = [self->v viewWithTag:2];
    b.layer.borderColor = [UIColor blackColor].CGColor;
    
}
- (IBAction)showStyles:(id)sender {
    self->v.hidden = !self->v.hidden;
}
- (IBAction)setFont:(id)sender {
    NSInteger i = ((UIButton*)sender).tag;
    ii = i;
    [self F1];
}

-(void)F1{
    if(ii == 1){
        self->bgColor = @"#8DA39E";
        self->txtColor = @"#000000";
    }else if(ii == 2){
        self->bgColor = @"#ffffff";
        self->txtColor = @"#000000";
    }else if(ii == 3){
        self->bgColor = @"#E0E0E0";
        self->txtColor = @"#000000";
    }else if(ii == 4){
        self->bgColor = @"#804000";
        self->txtColor = @"#ffffff";
    }else if(ii == 5){
        self->bgColor = @"#808000";
        self->txtColor = @"#ffffff";
    }
    
    if(jj == 11){
        self->font = @"Roboto";
        //self->font = @"Courier New";
    }else if(jj == 12){
        self->font = @"Times New Roman";
    }else if(jj == 13){
        self->font = @"Georgia";
    }else if(jj == 14){
        self->font = @"Courier New";
    }
    
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.body.style.background = '%@';", self->bgColor]];
    NSString *JSInjection = [NSString stringWithFormat:@"javascript: var els = document.getElementsByTagName('*'); if (els) {var i;for (i=0; i<els; i++) {els[i].style.color = '%@';}}",self->txtColor];
    [webView stringByEvaluatingJavaScriptFromString:JSInjection];
    
    
    //NSString *setFont = [NSString stringWithFormat:@"addCSSRule('html, body, div, p, span, a', 'font-family: arial;')"];
    //NSString *setFont = [NSString stringWithFormat:@"addCSSRule('body', 'font-family:%@ !important;')", self->font];
    //[webView stringByEvaluatingJavaScriptFromString:setFont];
//    NSString *cssString = [NSString stringWithFormat:@"body { font-family: '%@' !important; }",self->font ];//@"body { font-family: Helvetica; font-size: 50px }"; // 1
//    NSString *javascriptString = @"javascript: var style = document.createElement('style'); style.innerHTML = '%@'; document.head.appendChild(style);"; // 2
//    NSString *javascriptWithCSSString = [NSString stringWithFormat:javascriptString, cssString]; // 3
//    [webView stringByEvaluatingJavaScriptFromString:javascriptWithCSSString]; // 4

    
//    NSString *JSInjection2 = [NSString stringWithFormat:@"javascript: var els = document.getElementsByTagName('*'); if (els) {var i;for (i=0; i<els; i++) {els[i].style.fontFamily = '%@';}}",self->font];
//    [webView stringByEvaluatingJavaScriptFromString:JSInjection2];
    
    
//    NSString *js = [NSString stringWithFormat:@"var styleNode = document.createElement('style');"
//                    "styleNode.type = 'text/css';"
//                    "styleNode.innerHTML = 'body{font-family:\"%@\" !important;background-color:green !important;}';", self->font];
//    
//    js = [NSString stringWithFormat:@"%@document.getElementsByTagName('head')[0].appendChild(styleNode);", js];
//    
//    [webView stringByEvaluatingJavaScriptFromString:js];
}
- (IBAction)setFontMain:(id)sender {
    NSInteger i = ((UIButton*)sender).tag;
    jj = i;
    [self F1];
}

- (void) chapterDidFinishLoad:(Chapter *)chapter{
    totalPagesCount+=chapter.pageCount;
    
    if(chapter.chapterIndex + 1 < [loadedEpub.spineArray count]){
        [[loadedEpub.spineArray objectAtIndex:chapter.chapterIndex+1] setDelegate:self];
        [[loadedEpub.spineArray objectAtIndex:chapter.chapterIndex+1] loadChapterWithWindowSize:webView.bounds fontPercentSize:currentTextSize];
        [currentPageLabel setText:[NSString stringWithFormat:@"?/%d", totalPagesCount]];
    } else {
        [currentPageLabel setText:[NSString stringWithFormat:@"%d/%d",[self getGlobalPageCount], totalPagesCount]];
        [pageSlider setValue:(float)100*(float)[self getGlobalPageCount]/(float)totalPagesCount animated:YES];
        paginating = NO;
        NSLog(@"Pagination Ended!");
        [self loadSpine:1 atPageIndex:0];
    }
}

- (int) getGlobalPageCount{
    int pageCount = 0;
    for(int i=0; i<currentSpineIndex; i++){
        pageCount+= [[loadedEpub.spineArray objectAtIndex:i] pageCount];
    }
    pageCount+=currentPageInSpineIndex+1;
    return pageCount;
}

- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex {
    [self loadSpine:spineIndex atPageIndex:pageIndex highlightSearchResult:nil];
}

- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex highlightSearchResult:(SearchResult*)theResult{
    
    webView.hidden = YES;
    
    self.currentSearchResult = theResult;
    
    [chaptersPopover dismissPopoverAnimated:YES];
    [searchResultsPopover dismissPopoverAnimated:YES];
    
    NSURL* url = [NSURL fileURLWithPath:[[loadedEpub.spineArray objectAtIndex:spineIndex] spinePath]];
    
    
    NSString* content = [NSString stringWithContentsOfFile:url
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    
    NSString *html = @"<style>body{background-color:red;}</style>";
    //html = @"";
    
    content = [content stringByReplacingOccurrencesOfString:@"</head>"
                                                 withString:[NSString stringWithFormat:@"%@%@",html,@"</head>" ] ];
    
    
    //[webView loadHTMLString:content baseURL:nil];
    [webView loadRequest:[NSURLRequest requestWithURL:url]];
    currentPageInSpineIndex = pageIndex;
    currentSpineIndex = spineIndex;
    if(!paginating){
        [currentPageLabel setText:[NSString stringWithFormat:@"%d/%d",[self getGlobalPageCount], totalPagesCount]];
        [pageSlider setValue:(float)100*(float)[self getGlobalPageCount]/(float)totalPagesCount animated:YES];
    }
}


- (void) gotoPageInCurrentSpine:(int)pageIndex{
    if(pageIndex>=pagesInCurrentSpineCount){
        pageIndex = pagesInCurrentSpineCount - 1;
        currentPageInSpineIndex = pagesInCurrentSpineCount - 1;
    }
    
    float pageOffset = pageIndex*webView.bounds.size.width;
    
    NSString* goToOffsetFunc = [NSString stringWithFormat:@" function pageScroll(xOffset){ window.scroll(xOffset,0); } "];
    NSString* goTo =[NSString stringWithFormat:@"pageScroll(%f)", pageOffset];
    
    [webView stringByEvaluatingJavaScriptFromString:goToOffsetFunc];
    [webView stringByEvaluatingJavaScriptFromString:goTo];
    
    if(!paginating){
        [currentPageLabel setText:[NSString stringWithFormat:@"%d/%d",[self getGlobalPageCount], totalPagesCount]];
        [pageSlider setValue:(float)100*(float)[self getGlobalPageCount]/(float)totalPagesCount animated:YES];
    }
    
    webView.hidden = NO;
    
}

- (void) gotoNextSpine {
    if(!paginating){
        if(currentSpineIndex+1<[loadedEpub.spineArray count]){
            [self loadSpine:++currentSpineIndex atPageIndex:0];
        }
    }
}

- (void) gotoPrevSpine {
    if(!paginating){
        if(currentSpineIndex-1>=0){
            [self loadSpine:--currentSpineIndex atPageIndex:0];
        }
    }
}

- (void) gotoNextPage {
    if(!paginating){
        if(currentPageInSpineIndex+1<pagesInCurrentSpineCount){
            [self gotoPageInCurrentSpine:++currentPageInSpineIndex];
        } else {
            [self gotoNextSpine];
        }
    }
}

- (void) gotoPrevPage {
    if (!paginating) {
        if(currentPageInSpineIndex-1>=0){
            [self gotoPageInCurrentSpine:--currentPageInSpineIndex];
        } else {
            if(currentSpineIndex!=0){
                int targetPage = [[loadedEpub.spineArray objectAtIndex:(currentSpineIndex-1)] pageCount];
                [self loadSpine:--currentSpineIndex atPageIndex:targetPage-1];
            }
        }
    }
}


- (IBAction) increaseTextSizeClicked:(id)sender{
    if(!paginating){
        if(currentTextSize+25<=200){
            currentTextSize+=25;
            [self updatePagination];
            if(currentTextSize == 200){
                [incTextSizeButton setEnabled:NO];
            }
            [decTextSizeButton setEnabled:YES];
        }
    }
}
- (IBAction) decreaseTextSizeClicked:(id)sender{
    if(!paginating){
        if(currentTextSize-25>=50){
            currentTextSize-=25;
            [self updatePagination];
            if(currentTextSize==50){
                [decTextSizeButton setEnabled:NO];
            }
            [incTextSizeButton setEnabled:YES];
        }
    }
}

- (IBAction) doneClicked:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}


- (IBAction) slidingStarted:(id)sender{
    int targetPage = ((pageSlider.value/(float)100)*(float)totalPagesCount);
    if (targetPage==0) {
        targetPage++;
    }
    [currentPageLabel setText:[NSString stringWithFormat:@"%d/%d", targetPage, totalPagesCount]];
}

- (IBAction) slidingEnded:(id)sender{
    int targetPage = (int)((pageSlider.value/(float)100)*(float)totalPagesCount);
    if (targetPage==0) {
        targetPage++;
    }
    int pageSum = 0;
    int chapterIndex = 0;
    int pageIndex = 0;
    for(chapterIndex=0; chapterIndex<[loadedEpub.spineArray count]; chapterIndex++){
        pageSum+=[[loadedEpub.spineArray objectAtIndex:chapterIndex] pageCount];
        //		NSLog(@"Chapter %d, targetPage: %d, pageSum: %d, pageIndex: %d", chapterIndex, targetPage, pageSum, (pageSum-targetPage));
        if(pageSum>=targetPage){
            pageIndex = [[loadedEpub.spineArray objectAtIndex:chapterIndex] pageCount] - 1 - pageSum + targetPage;
            break;
        }
    }
    [self loadSpine:chapterIndex atPageIndex:pageIndex];
}

- (IBAction) showChapterIndex:(id)sender{
    if (!chaptersPopover) {
        ChapterListViewController *chapterListView = [ChapterListViewController new];
        [chapterListView setEpubViewController:self];
        chaptersPopover = [[WYPopoverController alloc] initWithContentViewController:chapterListView];
        [chaptersPopover setPopoverContentSize:CGSizeMake(self.view.frame.size.width - 24, self.webView.frame.size.height)];
    }
    if ([chaptersPopover isPopoverVisible]) {
        [chaptersPopover dismissPopoverAnimated:YES];
    }
    else {
        [chaptersPopover presentPopoverFromBarButtonItem:self.chapterListButton
                                permittedArrowDirections:WYPopoverArrowDirectionAny
                                                animated:YES];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)theWebView{
    
    NSString *varMySheet = @"var mySheet = document.styleSheets[0];";
    
    NSString *addCSSRule =  @"function addCSSRule(selector, newRule) {"
    "if (mySheet.addRule) {"
    "mySheet.addRule(selector, newRule);"								// For Internet Explorer
    "} else {"
    "ruleIndex = mySheet.cssRules.length;"
    "mySheet.insertRule(selector + '{' + newRule + ';}', ruleIndex);"   // For Firefox, Chrome, etc.
    "}"
    "}";
    
    
    NSString *insertRule1 = [NSString stringWithFormat:@"addCSSRule('html', 'padding: 0px; height: %fpx; -webkit-column-gap: 0px; -webkit-column-width: %fpx;')", webView.frame.size.height, webView.frame.size.width];
    NSString *insertRule2 = [NSString stringWithFormat:@"addCSSRule('p', 'text-align: justify;')"];
    NSString *setTextSizeRule = [NSString stringWithFormat:@"addCSSRule('body', '-webkit-text-size-adjust: %d%%;')", currentTextSize];
    NSString *setHighlightColorRule = [NSString stringWithFormat:@"addCSSRule('highlight', 'background-color: yellow;')"];
    
    [webView stringByEvaluatingJavaScriptFromString:varMySheet];
    
    [webView stringByEvaluatingJavaScriptFromString:addCSSRule];
    
    [webView stringByEvaluatingJavaScriptFromString:insertRule1];
    
    [webView stringByEvaluatingJavaScriptFromString:insertRule2];
    
    [webView stringByEvaluatingJavaScriptFromString:setTextSizeRule];
    
    [webView stringByEvaluatingJavaScriptFromString:setHighlightColorRule];
    
    if(currentSearchResult!=nil){
        //	NSLog(@"Highlighting %@", currentSearchResult.originatingQuery);
        [webView highlightAllOccurencesOfString:currentSearchResult.originatingQuery];
    }
    
    //NSString *yourHTMLSourceCodeString = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('html')[0].innerHTML"];
    
    [self F1];
    
    int totalWidth = [[webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.scrollWidth"] intValue];
    pagesInCurrentSpineCount = (int)((float)totalWidth/webView.bounds.size.width);
    
    [self gotoPageInCurrentSpine:currentPageInSpineIndex];
}

- (void) updatePagination{
    if(epubLoaded){
        if(!paginating){
            NSLog(@"Pagination Started!");
            paginating = YES;
            totalPagesCount=0;
            [self loadSpine:currentSpineIndex atPageIndex:currentPageInSpineIndex];
            [[loadedEpub.spineArray objectAtIndex:0] setDelegate:self];
            [[loadedEpub.spineArray objectAtIndex:0] loadChapterWithWindowSize:webView.bounds fontPercentSize:currentTextSize];
            [currentPageLabel setText:@"?/?"];
        }
    }
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    if (!searchResultsPopover) {
        searchResultsPopover = [[WYPopoverController alloc] initWithContentViewController:searchResViewController];
        [searchResultsPopover setPopoverContentSize:CGSizeMake(400, 600)];
    }
    if (![searchResultsPopover isPopoverVisible]) {
        [searchResultsPopover presentPopoverFromRect:searchBar.bounds
                                              inView:searchBar
                            permittedArrowDirections:WYPopoverArrowDirectionAny
                                            animated:YES];
    }
    
    if (!self.searching){
        self.searching = YES;
        [searchResViewController searchString:[searchBar text]];
        [searchBar resignFirstResponder];
    }
}



#pragma mark -
#pragma mark Rotation support

// Ensure that the view controller supports rotation and that the split view can therefore show in both portrait and landscape.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    NSLog(@"shouldAutorotate");
    [self updatePagination];
    return YES;
}

#pragma mark -
#pragma mark View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self->bgColor = @"#ffffff";
    self->txtColor = @"#000000";
    self->font = @"Roboto";
    
    [webView setDelegate:self];
    UIScrollView* sv = nil;
    for (UIView* v in  webView.subviews) {
        if([v isKindOfClass:[UIScrollView class]]){
            sv = (UIScrollView*) v;
            sv.scrollEnabled = NO;
            sv.bounces = NO;
        }
    }
    currentTextSize = 100;
    
    UISwipeGestureRecognizer* rightSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoNextPage)] ;
    [rightSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
    
    UISwipeGestureRecognizer* leftSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoPrevPage)] ;
    [leftSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
    
    [webView addGestureRecognizer:rightSwipeRecognizer];
    [webView addGestureRecognizer:leftSwipeRecognizer];
    
    [pageSlider setThumbImage:[UIImage imageNamed:@"slider_ball.png"] forState:UIControlStateNormal];
    [pageSlider setMinimumTrackImage:[[UIImage imageNamed:@"orangeSlide.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
    [pageSlider setMaximumTrackImage:[[UIImage imageNamed:@"yellowSlide.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
    
    searchResViewController = [[SearchResultsViewController alloc] initWithNibName:@"SearchResultsViewController" bundle:[NSBundle mainBundle]];
    searchResViewController.epubViewController = self;
}

- (void)viewDidUnload {
    self.toolbar = nil;
    self.webView = nil;
    self.chapterListButton = nil;
    self.decTextSizeButton = nil;
    self.incTextSizeButton = nil;
    self.pageSlider = nil;
    self.currentPageLabel = nil;
}


#pragma mark -
#pragma mark Memory management

/*
 - (void)didReceiveMemoryWarning {
 // Releases the view if it doesn't have a superview.
 [super didReceiveMemoryWarning];
 
 // Release any cached data, images, etc that aren't in use.
 }
 */

- (void)dealloc {
    self.toolbar = nil;
    self.webView = nil;
    self.chapterListButton = nil;
    self.decTextSizeButton = nil;
    self.incTextSizeButton = nil;
    self.pageSlider = nil;
    self.currentPageLabel = nil;
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}
@end

//  DetailViewController.m
//  AePubReader
//
//  Created by Federico Frappi on 04/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.


//#import "EPubViewController.h"
//#import "ChapterListViewController.h"
//#import "SearchResultsViewController.h"
//#import "SearchResult.h"
//#import "UIWebView+SearchWebView.h"
//#import "Chapter.h"
//
//@interface EPubViewController()
//
//
//- (void) gotoNextSpine;
//- (void) gotoPrevSpine;
//- (void) gotoNextPage;
//- (void) gotoPrevPage;
//
//- (int) getGlobalPageCount;
//
//- (void) gotoPageInCurrentSpine: (int)pageIndex;
//- (void) updatePagination;
//
//- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex;
//
//
//@end
//
//@implementation EPubViewController
//
//@synthesize loadedEpub, toolbar, webView;
//@synthesize chapterListButton, decTextSizeButton, incTextSizeButton;
//@synthesize currentPageLabel, pageSlider, searching;
//@synthesize currentSearchResult;
//
//#pragma mark -
//
//- (void) loadEpub:(NSURL*) epubURL{
//    currentSpineIndex = 0;
//    currentPageInSpineIndex = 0;
//    pagesInCurrentSpineCount = 0;
//    totalPagesCount = 0;
//    searching = NO;
//    epubLoaded = NO;
//    self.loadedEpub = [[EPub alloc] initWithEPubPath:[epubURL path]];
//    epubLoaded = YES;
//    NSLog(@"loadEpub");
//    [self updatePagination];
//}
//
//- (void) chapterDidFinishLoad:(Chapter *)chapter{
//    totalPagesCount+=chapter.pageCount;
//
//    if(chapter.chapterIndex + 1 < [loadedEpub.spineArray count]){
//        [[loadedEpub.spineArray objectAtIndex:chapter.chapterIndex+1] setDelegate:self];
//        [[loadedEpub.spineArray objectAtIndex:chapter.chapterIndex+1] loadChapterWithWindowSize:webView.bounds fontPercentSize:currentTextSize];
//        [currentPageLabel setText:[NSString stringWithFormat:@"?/%d", totalPagesCount]];
//    } else {
//        [currentPageLabel setText:[NSString stringWithFormat:@"%d/%d",[self getGlobalPageCount], totalPagesCount]];
//        [pageSlider setValue:(float)100*(float)[self getGlobalPageCount]/(float)totalPagesCount animated:YES];
//        paginating = NO;
//        NSLog(@"Pagination Ended!");
//    }
//}
//
//- (int) getGlobalPageCount{
//    int pageCount = 0;
//    for(int i=0; i<currentSpineIndex; i++){
//        pageCount+= [[loadedEpub.spineArray objectAtIndex:i] pageCount];
//    }
//    pageCount+=currentPageInSpineIndex+1;
//    return pageCount;
//}
//
//- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex {
//    [self loadSpine:spineIndex atPageIndex:pageIndex highlightSearchResult:nil];
//}
//
//- (void) loadSpine:(int)spineIndex atPageIndex:(int)pageIndex highlightSearchResult:(SearchResult*)theResult{
//
//    webView.hidden = YES;
//
//    self.currentSearchResult = theResult;
//
//    [chaptersPopover dismissPopoverAnimated:YES];
//    [searchResultsPopover dismissPopoverAnimated:YES];
//
//    NSURL* url = [NSURL fileURLWithPath:[[loadedEpub.spineArray objectAtIndex:spineIndex] spinePath]];
//    [webView loadRequest:[NSURLRequest requestWithURL:url]];
//    currentPageInSpineIndex = pageIndex;
//    currentSpineIndex = spineIndex;
//    if(!paginating){
//        [currentPageLabel setText:[NSString stringWithFormat:@"%d/%d",[self getGlobalPageCount], totalPagesCount]];
//        [pageSlider setValue:(float)100*(float)[self getGlobalPageCount]/(float)totalPagesCount animated:YES];
//    }
//}
//
//- (void) gotoPageInCurrentSpine:(int)pageIndex{
//    if(pageIndex>=pagesInCurrentSpineCount){
//        pageIndex = pagesInCurrentSpineCount - 1;
//        currentPageInSpineIndex = pagesInCurrentSpineCount - 1;
//    }
//
//    float pageOffset = pageIndex*webView.bounds.size.width;
//
//    NSString* goToOffsetFunc = [NSString stringWithFormat:@" function pageScroll(xOffset){ window.scroll(xOffset,0); } "];
//    NSString* goTo =[NSString stringWithFormat:@"pageScroll(%f)", pageOffset];
//
//    [webView stringByEvaluatingJavaScriptFromString:goToOffsetFunc];
//    [webView stringByEvaluatingJavaScriptFromString:goTo];
//
//    if(!paginating){
//        [currentPageLabel setText:[NSString stringWithFormat:@"%d/%d",[self getGlobalPageCount], totalPagesCount]];
//        [pageSlider setValue:(float)100*(float)[self getGlobalPageCount]/(float)totalPagesCount animated:YES];
//    }
//
//    webView.hidden = NO;
//
//}
//
//- (void) gotoNextSpine {
//    if(!paginating){
//        if(currentSpineIndex+1<[loadedEpub.spineArray count]){
//            [self loadSpine:++currentSpineIndex atPageIndex:0];
//        }
//    }
//}
//
//- (void) gotoPrevSpine {
//    if(!paginating){
//        if(currentSpineIndex-1>=0){
//            [self loadSpine:--currentSpineIndex atPageIndex:0];
//        }
//    }
//}
//
//- (void) gotoNextPage {
//    if(!paginating){
//        if(currentPageInSpineIndex+1<pagesInCurrentSpineCount){
//            [self gotoPageInCurrentSpine:++currentPageInSpineIndex];
//        } else {
//            [self gotoNextSpine];
//        }
//    }
//}
//
//- (void) gotoPrevPage {
//    if (!paginating) {
//        if(currentPageInSpineIndex-1>=0){
//            [self gotoPageInCurrentSpine:--currentPageInSpineIndex];
//        } else {
//            if(currentSpineIndex!=0){
//                int targetPage = [[loadedEpub.spineArray objectAtIndex:(currentSpineIndex-1)] pageCount];
//                [self loadSpine:--currentSpineIndex atPageIndex:targetPage-1];
//            }
//        }
//    }
//}
//
//
//- (IBAction) increaseTextSizeClicked:(id)sender{
//    if(!paginating){
//        if(currentTextSize+25<=200){
//            currentTextSize+=25;
//            [self updatePagination];
//            if(currentTextSize == 200){
//                [incTextSizeButton setEnabled:NO];
//            }
//            [decTextSizeButton setEnabled:YES];
//        }
//    }
//}
//- (IBAction) decreaseTextSizeClicked:(id)sender{
//    if(!paginating){
//        if(currentTextSize-25>=50){
//            currentTextSize-=25;
//            [self updatePagination];
//            if(currentTextSize==50){
//                [decTextSizeButton setEnabled:NO];
//            }
//            [incTextSizeButton setEnabled:YES];
//        }
//    }
//}
//
//- (IBAction) doneClicked:(id)sender{
//    [self dismissModalViewControllerAnimated:YES];
//}
//
//
//- (IBAction) slidingStarted:(id)sender{
//    int targetPage = ((pageSlider.value/(float)100)*(float)totalPagesCount);
//    if (targetPage==0) {
//        targetPage++;
//    }
//    [currentPageLabel setText:[NSString stringWithFormat:@"%d/%d", targetPage, totalPagesCount]];
//}
//
//- (IBAction) slidingEnded:(id)sender{
//    int targetPage = (int)((pageSlider.value/(float)100)*(float)totalPagesCount);
//    if (targetPage==0) {
//        targetPage++;
//    }
//    int pageSum = 0;
//    int chapterIndex = 0;
//    int pageIndex = 0;
//    for(chapterIndex=0; chapterIndex<[loadedEpub.spineArray count]; chapterIndex++){
//        pageSum+=[[loadedEpub.spineArray objectAtIndex:chapterIndex] pageCount];
//        //		NSLog(@"Chapter %d, targetPage: %d, pageSum: %d, pageIndex: %d", chapterIndex, targetPage, pageSum, (pageSum-targetPage));
//        if(pageSum>=targetPage){
//            pageIndex = [[loadedEpub.spineArray objectAtIndex:chapterIndex] pageCount] - 1 - pageSum + targetPage;
//            break;
//        }
//    }
//    [self loadSpine:chapterIndex atPageIndex:pageIndex];
//}
//
//- (IBAction) showChapterIndex:(id)sender{
//    if(chaptersPopover==nil){
//        ChapterListViewController* chapterListView = [[ChapterListViewController alloc] initWithNibName:@"ChapterListViewController" bundle:[NSBundle mainBundle]];
//        [chapterListView setEpubViewController:self];
//        chaptersPopover = [[UIPopoverController alloc] initWithContentViewController:chapterListView];
//        [chaptersPopover setPopoverContentSize:CGSizeMake(400, 600)];
//        //[chapterListView release];
//    }
//    if ([chaptersPopover isPopoverVisible]) {
//        [chaptersPopover dismissPopoverAnimated:YES];
//    }else{
//        [chaptersPopover presentPopoverFromBarButtonItem:chapterListButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//    }
//}
////- (IBAction) showChapterIndex:(id)sender{
////    if (!chaptersPopover) {
////        ChapterListViewController *chapterListView = [ChapterListViewController new];
////        [chapterListView setEpubViewController:self];
////        chaptersPopover = [[WYPopoverController alloc] initWithContentViewController:chapterListView];
////        [chaptersPopover setPopoverContentSize:CGSizeMake(400, 600)];
////    }
////    if ([chaptersPopover isPopoverVisible]) {
////        [chaptersPopover dismissPopoverAnimated:YES];
////    }
////    else {
////        [chaptersPopover presentPopoverFromBarButtonItem:self.chapterListButton
////                                permittedArrowDirections:WYPopoverArrowDirectionAny
////                                                animated:YES];
////    }
////}
//
//- (void)webViewDidFinishLoad:(UIWebView *)theWebView{
//
//    NSString *varMySheet = @"var mySheet = document.styleSheets[0];";
//
//    NSString *addCSSRule =  @"function addCSSRule(selector, newRule) {"
//    "if (mySheet.addRule) {"
//    "mySheet.addRule(selector, newRule);"								// For Internet Explorer
//    "} else {"
//    "ruleIndex = mySheet.cssRules.length;"
//    "mySheet.insertRule(selector + '{' + newRule + ';}', ruleIndex);"   // For Firefox, Chrome, etc.
//    "}"
//    "}";
//
//    NSString *insertRule1 = [NSString stringWithFormat:@"addCSSRule('html', 'padding: 0px; height: %fpx; -webkit-column-gap: 0px; -webkit-column-width: %fpx;')", webView.frame.size.height, webView.frame.size.width];
//    NSString *insertRule2 = [NSString stringWithFormat:@"addCSSRule('p', 'text-align: justify;')"];
//    NSString *setTextSizeRule = [NSString stringWithFormat:@"addCSSRule('body', '-webkit-text-size-adjust: %d%%;')", currentTextSize];
//    NSString *setHighlightColorRule = [NSString stringWithFormat:@"addCSSRule('highlight', 'background-color: yellow;')"];
//
//
//    [webView stringByEvaluatingJavaScriptFromString:varMySheet];
//
//    [webView stringByEvaluatingJavaScriptFromString:addCSSRule];
//
//    [webView stringByEvaluatingJavaScriptFromString:insertRule1];
//
//    [webView stringByEvaluatingJavaScriptFromString:insertRule2];
//
//    [webView stringByEvaluatingJavaScriptFromString:setTextSizeRule];
//
//    [webView stringByEvaluatingJavaScriptFromString:setHighlightColorRule];
//
//    if(currentSearchResult!=nil){
//        //	NSLog(@"Highlighting %@", currentSearchResult.originatingQuery);
//        [webView highlightAllOccurencesOfString:currentSearchResult.originatingQuery];
//    }
//
//
//    int totalWidth = [[webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.scrollWidth"] intValue];
//    pagesInCurrentSpineCount = (int)((float)totalWidth/webView.bounds.size.width);
//
//    [self gotoPageInCurrentSpine:currentPageInSpineIndex];
//}
//
//- (void) updatePagination{
//    if(epubLoaded){
//        if(!paginating){
//            NSLog(@"Pagination Started!");
//            paginating = YES;
//            totalPagesCount=0;
//            [self loadSpine:currentSpineIndex atPageIndex:currentPageInSpineIndex];
//            [[loadedEpub.spineArray objectAtIndex:0] setDelegate:self];
//            [[loadedEpub.spineArray objectAtIndex:0] loadChapterWithWindowSize:webView.bounds fontPercentSize:currentTextSize];
//            [currentPageLabel setText:@"?/?"];
//        }
//    }
//}
//
//
//- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
//    if(searchResultsPopover==nil){
//        searchResultsPopover = [[UIPopoverController alloc] initWithContentViewController:searchResViewController];
//        [searchResultsPopover setPopoverContentSize:CGSizeMake(400, 600)];
//    }
//    if (![searchResultsPopover isPopoverVisible]) {
//        [searchResultsPopover presentPopoverFromRect:searchBar.bounds inView:searchBar permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//    }
//    //	NSLog(@"Searching for %@", [searchBar text]);
//    if(!searching){
//        searching = YES;
//        [searchResViewController searchString:[searchBar text]];
//        [searchBar resignFirstResponder];
//    }
//}
//
//
//#pragma mark -
//#pragma mark Rotation support
//
//// Ensure that the view controller supports rotation and that the split view can therefore show in both portrait and landscape.
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    NSLog(@"shouldAutorotate");
//    [self updatePagination];
//    return YES;
//}
//
//#pragma mark -
//#pragma mark View lifecycle
//
//// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    [webView setDelegate:self];
//
//    UIScrollView* sv = nil;
//    for (UIView* v in  webView.subviews) {
//        if([v isKindOfClass:[UIScrollView class]]){
//            sv = (UIScrollView*) v;
//            sv.scrollEnabled = NO;
//            sv.bounces = NO;
//        }
//    }
//    currentTextSize = 100;
//
//    UISwipeGestureRecognizer* rightSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoNextPage)] ;
//    //UISwipeGestureRecognizer* rightSwipeRecognizer = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoNextPage)] autorelease];
//    [rightSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
//
//    UISwipeGestureRecognizer* leftSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoPrevPage)] ;
//    //UISwipeGestureRecognizer* leftSwipeRecognizer = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gotoPrevPage)] autorelease];
//    [leftSwipeRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
//
//    [webView addGestureRecognizer:rightSwipeRecognizer];
//    [webView addGestureRecognizer:leftSwipeRecognizer];
//
//    [pageSlider setThumbImage:[UIImage imageNamed:@"slider_ball.png"] forState:UIControlStateNormal];
//    [pageSlider setMinimumTrackImage:[[UIImage imageNamed:@"orangeSlide.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
//    [pageSlider setMaximumTrackImage:[[UIImage imageNamed:@"yellowSlide.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
//
//    searchResViewController = [[SearchResultsViewController alloc] initWithNibName:@"SearchResultsViewController" bundle:[NSBundle mainBundle]];
//    searchResViewController.epubViewController = self;
//}
//
//- (void)viewDidUnload {
//    self.toolbar = nil;
//    self.webView = nil;
//    self.chapterListButton = nil;
//    self.decTextSizeButton = nil;
//    self.incTextSizeButton = nil;
//    self.pageSlider = nil;
//    self.currentPageLabel = nil;
//}
//
//
//#pragma mark -
//#pragma mark Memory management
//
///*
// - (void)didReceiveMemoryWarning {
// // Releases the view if it doesn't have a superview.
// [super didReceiveMemoryWarning];
//
// // Release any cached data, images, etc that aren't in use.
// }
// */
//
////- (void)dealloc {
////    self.toolbar = nil;
////    self.webView = nil;
////    self.chapterListButton = nil;
////    self.decTextSizeButton = nil;
////    self.incTextSizeButton = nil;
////    self.pageSlider = nil;
////    self.currentPageLabel = nil;
////    [loadedEpub release];
////    [chaptersPopover release];
////    [searchResultsPopover release];
////    [searchResViewController release];
////    [currentSearchResult release];
////    [super dealloc];
////}
//
//- (void)dealloc {
//    self.toolbar = nil;
//    self.webView = nil;
//    self.chapterListButton = nil;
//    self.decTextSizeButton = nil;
//    self.incTextSizeButton = nil;
//    self.pageSlider = nil;
//    self.currentPageLabel = nil;
//}
//
//@end
