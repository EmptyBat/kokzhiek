//
//  SocialAuth.m
//  Kokzhiek
//
//  Created by Admin on 09.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "SocialAuth.h"
#import "Auth.h"
#import "SWRevealViewController.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "VkontakteDelegate.h"
#import "constants.h"
#import "SocialAuth.h"

@interface SocialAuth ()

@end

@implementation SocialAuth

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.txtEmail.text = self.Email;
    self.txtFio.text = self.Name;
    
    self.txtEmail.delegate = self;
    self.txtFio.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)registerAction:(id)sender {
    
    if(![self.txtEmail.text isEqualToString:@""] && ![self.txtFio.text isEqualToString:@""]){
        NSString*url=[NSString stringWithFormat:@"%@kk/mobile/socialauth?provider_user_id=%@&provider=%@&password=kokzhiek062016&name=%@&email=%@",kBaseURL,self.Id,self.Provider,self.txtFio.text,self.txtEmail.text];
        
        NSLog(@"%@",url);
        [SVProgressHUD show];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [SVProgressHUD dismiss];
            
            [[NSUserDefaults standardUserDefaults] setObject:self.txtEmail.text forKey:@"email"];
            [[NSUserDefaults standardUserDefaults] setObject:@"kokzhiek062016" forKey:@"password"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"address"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"avatar"];
            [[NSUserDefaults standardUserDefaults] setObject:self.txtFio.text forKey:@"name"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"phone"];
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"issocial"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"Profile"];
            [[self navigationController] pushViewController:viewController animated:YES];
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [SVProgressHUD dismiss];
        }];
    }else{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = NSLocalizedString(@"PLEASE_FILL_IN", @"");
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        
        [hud hide:YES afterDelay:2];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField==self.txtEmail){
        [self.txtFio becomeFirstResponder];
    }
    else  {
        [theTextField resignFirstResponder];
    }
    return YES;
}
@end
