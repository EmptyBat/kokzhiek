//
//  Location.m
//  JSONHandler
//
//  Created by Phillipus on 28/10/2013.
//  Copyright (c) 2013 Dada Beatnik. All rights reserved.
//

#import "Location.h"

@implementation Location

// Init the object with information from a dictionary
- (id)initWithJSONDictionary:(NSDictionary *)jsonDictionary {
    if(self = [self init]) {
        // Assign all properties with keyed values from the dictionary
        _name = [jsonDictionary objectForKey:@"name"];
        _I_id = [jsonDictionary objectForKey:@"I_id"];
        _link= [jsonDictionary objectForKey:@"link"];
        _icon_name = [jsonDictionary objectForKey:@"icon_name"];
       _image= [jsonDictionary objectForKey:@"image"];
        _categories = [jsonDictionary objectForKey:@"categories"];
        _check= [jsonDictionary objectForKey:@"check"];
        _rating = [jsonDictionary objectForKey:@"rating"];
        _company_name= [jsonDictionary objectForKey:@"company_name"];
        _address= [jsonDictionary objectForKey:@"address"];
        _category_link=[jsonDictionary objectForKey:@"category_link"];
        _latitude= [jsonDictionary objectForKey:@"latitude"];
        _longitude=[jsonDictionary objectForKey:@"longitude"];
         if ([jsonDictionary objectForKey:@"name"] == [NSNull null]) { _name=nil;}
         if ([jsonDictionary objectForKey:@"I_id"] == [NSNull null]) { _I_id=nil;}
         if ([jsonDictionary objectForKey:@"link"] == [NSNull null]) { _link=nil;}
         if ([jsonDictionary objectForKey:@"icon_name"] == [NSNull null]) { _icon_name=nil;}
        if ([jsonDictionary objectForKey:@"categories"] == [NSNull null]) { _categories=nil;}
        if ([jsonDictionary objectForKey:@"check"] == [NSNull null]) { _check=nil;}
        if ([jsonDictionary objectForKey:@"rating"] == [NSNull null]) { _rating=nil;}
        if ([jsonDictionary objectForKey:@"company_name"] == [NSNull null]) { _company_name=nil;}
        if ([jsonDictionary objectForKey:@"company_name"] == [NSNull null]) { _company_name=nil;}
        if ([jsonDictionary objectForKey:@"category_link"] == [NSNull null]) { _category_link=nil;}
        if ([jsonDictionary objectForKey:@"latitude"] == [NSNull null]) { _latitude=nil;}
        if ([jsonDictionary objectForKey:@"longitude"] == [NSNull null]) { _longitude=nil;}

    }
    
    return self;
    }

@end
