//
//  Location.h
//  JSONHandler
//
//  Created by Phillipus on 28/10/2013.
//  Copyright (c) 2013 Dada Beatnik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Location : NSObject

- (id)initWithJSONDictionary:(NSDictionary *)jsonDictionary;

@property (readonly) NSString *name;
@property (readonly) NSString *I_id;
@property (readonly) NSString *link;
@property (readonly) NSString *icon_name;
@property (readonly) NSString *image;
@property (readonly) NSString *categories;
@property (readonly) NSString *check;
@property (readonly) NSString *rating;
@property (readonly) NSString *company_name;
@property (readonly) NSString *address;
@property (readonly) NSString *category_link;
@property (readonly) NSString *latitude;
@property (readonly) NSString *longitude;
@end
