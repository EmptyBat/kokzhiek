//
//  BookDetail.h
//  Kokzhiek
//
//  Created by bugingroup on 03.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface BookDetail : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic)NSString*image_url;
@property(nonatomic)NSString*book_title;
@property(nonatomic)NSString*author;
@property(nonatomic)NSString*isbn;
@property(nonatomic)NSString*description_book;
@property(nonatomic)NSString*ebook_price;
@property(nonatomic)NSString*price;
@property(nonatomic)NSString*page;
@property(nonatomic)NSString*publish_year;
@property(nonatomic)NSString*formats;
@property(nonatomic)NSString*book_id;
@property(nonatomic)NSString*status;
@property (strong) NSMutableArray *devices;
@property (weak, nonatomic) IBOutlet UIButton *btnBookBuy2;
@property (weak, nonatomic) IBOutlet UIButton *btnBookBuy1;
@property (weak, nonatomic) IBOutlet UIButton *btnBookBuy3;
- (IBAction)save:(id)sender;
@property NSString *source;
@property NSInteger demo;
@end
