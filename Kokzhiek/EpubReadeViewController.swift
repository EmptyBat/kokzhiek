//
//  EpubReadeViewController.swift
//  Kokzhiek
//
//  Created by Admin on 23.07.2018.
//  Copyright © 2018 Almas. All rights reserved.
//

import UIKit
import FolioReaderKit

class EpubReaderViewController: FolioReaderContainer {
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            let config = FolioReaderConfig()
            config.scrollDirection = .horizontal
            config.shouldHideNavigationOnTap = true
            config.tintColor = UIColor(red: 0/255.0, green: 139/255.0, blue: 123/255.0, alpha: 1.0)
            config.displayTitle = true
            config.hidePageIndicator = false
            config.localizedReaderLessThanOneMinute = NSLocalizedString("LESSTHAN", comment: "Меньше минуты назад")
            config.localizedReaderOnePageLeft = NSLocalizedString("ONEPAGELEFT", comment: "Одна страница слева")
            config.localizedReaderManyPagesLeft = NSLocalizedString("MANYPAGELEFT", comment: "Страницы слева")
            config.localizedHighlightsTitle = NSLocalizedString("HighlightsTitle", comment: "Закладка")
            config.localizedContentsTitle = NSLocalizedString("CHAPTER", comment: "Главы")
            config.localizedFontMenuDay = NSLocalizedString("DAY", comment: "День")
            config.localizedFontMenuNight = NSLocalizedString("NIGHT", comment: "Ночь")
            config.localizedLayoutHorizontal = NSLocalizedString("HORIZONTAL", comment: "Горизональная")
            config.localizedLayoutVertical = NSLocalizedString("VERTICAL", comment: "Вертикальная")
            config.localizedDefineMenu = NSLocalizedString("DEFINE", comment: "Словарь")
            config.localizedHighlightMenu = NSLocalizedString("HighlightMenu", comment: "Закладка")
            config.localizedShareImageQuote = NSLocalizedString("ImageQuote", comment: "Изображение")
            config.localizedShareTextQuote = NSLocalizedString("ShareTextQuote", comment: "Текст")
            config.localizedCancel = NSLocalizedString("CANCEL", comment: "Отмена")
          //  config.tintColor = UIColor.blue
           // config.localizedHighlightsTitle  = "tessr"
            // Print the chapter ID if one was clicked
            // A chapter in "The Silver Chair" looks like this "<section class="chapter" title="Chapter I" epub:type="chapter" id="id70364673704880">"
            // To know if a user tapped on a chapter we can listen to events on the class "chapter" and receive the id value
            config.enableTTS = false
            let listener = ClassBasedOnClickListener(schemeName: "chaptertapped", querySelector: ".chapter", attributeName: "id", onClickAction: { (attributeContent: String?, touchPointRelativeToWebView: CGPoint?) in
                print("chapter with id: " + (attributeContent ?? "-") + " clicked")
            })
             let bookPath = Bundle.main.path(forResource: "The Silver Chair", ofType: "epub")
            print("assa",bookPath)
            print("assa====",  UserDefaults.standard.string(forKey: "book_url"))
            config.classBasedOnClickListeners.append(listener)
            if let book = UserDefaults.standard.string(forKey: "book_url"){
            setupConfig(config, epubPath: book)
        }
    }
}

