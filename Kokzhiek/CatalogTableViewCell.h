//
//  CatalogTableViewCell.h
//  Kokzhiek
//
//  Created by Almas Abdrasilov on 21.07.2018.
//  Copyright © 2018 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatalogTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *book_img;
@property (weak, nonatomic) IBOutlet UILabel *title_lb;
@property (weak, nonatomic) IBOutlet UILabel *desc_lb;
@property (weak, nonatomic) IBOutlet UILabel *price_lb;

@end
