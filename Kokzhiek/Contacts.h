//
//  Contacts.h
//  Kokzhiek
//
//  Created by Admin on 03.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Contacts : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *txtPhone;
@property (weak, nonatomic) IBOutlet UITextView *txtEmail;
@property (weak, nonatomic) IBOutlet UITextView *txtAddress;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *menu;
@end
