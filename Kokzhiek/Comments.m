//
//  Comments.m
//  Kokzhiek
//
//  Created by Admin on 04.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "Comments.h"
#import "Books_collection.h"
#import "AFNetworking.h"
#import <TLYShyNavBar/TLYShyNavBarManager.h>
#import "SWRevealViewController.h"
#import "SVProgressHUD.h"
#import "constants.h"
#import "ORGContainerCellView.h"
#import "ORGContainerCell.h"
#import "ORGArticleCollectionViewCell.h"
#import "BookDetail.h"
#import "EPubViewController.h"
#import "MBProgressHUD.h"
#import "FTWCache.h"
#import "NSString+MD5.h"
#import "SendComment.h"

@interface Comments (){
    NSArray*collection_data;
}
@end

@implementation Comments

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.layoutMargins = UIEdgeInsetsZero;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 70.0f;
    
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:true];
    [self getData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    static UITableViewCell *cell = nil;
//    static dispatch_once_t onceToken;
//    
//    dispatch_once(&onceToken, ^{
//        cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
//    });
//    
//    //[self setUpCell:cell atIndexPath:indexPath];
//    
//    return [self calculateHeightForConfiguredSizingCell:cell];
//}

//- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
//    [sizingCell layoutIfNeeded];
//    
//    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
//    return size.height;
//}
-(void)getData{
    //NSString*url=[NSString stringWithFormat:@"%@/kk/mobile/comment?book_id=%@&user_id=%@&comment=%@",kBaseURL,self->book_id,@"",self.txtText.text];
    NSString*url=[NSString stringWithFormat:@"%@kk/mobile/listcomment?book_id=%@",kBaseURL, self.book_id];
    NSLog(@"%@",url);
    [SVProgressHUD show];
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        [SVProgressHUD dismiss];
        
        collection_data= [responseObject objectForKey:@"result"];
        [self.tableView reloadData];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [SVProgressHUD dismiss];
    }];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"comment"]) {
        SendComment*vc = segue.destinationViewController;
        vc.book_id = self.book_id;
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(![collection_data isKindOfClass:[NSArray class]]) {
        return 0;
    }
    return [collection_data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSDictionary *dic = collection_data[indexPath.row];
    
    UITextView *txtV = (UITextView*)[cell viewWithTag:1];
    txtV.text = [dic objectForKey:@"comment"];
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [formatter dateFromString:[dic objectForKey:@"created_at"]];
    
    //NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSLocale *locale = [NSLocale currentLocale];
    
    [formatter setLocale:locale];
    [formatter setDateFormat:@"d MMMM, yyyy"];
    
    UILabel *lblD = (UILabel*)[cell viewWithTag:2];
    lblD.text = [formatter stringFromDate:date];
    
    cell.layoutMargins = UIEdgeInsetsZero;
    cell.preservesSuperviewLayoutMargins = false;
    return cell;
}

@end
