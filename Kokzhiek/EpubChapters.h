//
//  EpubChapters.h
//  Kokzhiek
//
//  Created by Admin on 16.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EpubChapters : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSArray *chapters;
@end
