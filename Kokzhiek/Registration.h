//
//  Registration.h
//  Kokzhiek
//
//  Created by bugingroup on 13.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Registration : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *return_password;
@property (strong, nonatomic) IBOutlet UIButton *registration;
- (IBAction)registration:(id)sender;
@end
