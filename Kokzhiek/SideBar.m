//
//  SideBar.m
//  Kokzhiek
//
//  Created by bugingroup on 03.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "SideBar.h"

@interface SideBar ()

{
    NSArray *menuTtls;
    NSArray *menuImgs;
}

@end

@implementation SideBar

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    menuTtls = @[NSLocalizedString(@"MENU_MAIN",""),NSLocalizedString(@"MENU_CATALOG",""),NSLocalizedString(@"MENU_MY_BOOKS",""),
                 NSLocalizedString(@"MENU_PROFILE",""), NSLocalizedString(@"MENU_CONTACT",""),NSLocalizedString(@"MENU_ABOUT",""),NSLocalizedString(@"MENU_SHARE","")];
    
    menuImgs = @[@"ic_bastybet",@"ic_kitapxana",@"menu_book", @"ic_user",@"black_phone",@"ic_baptaular",@"share-1"];
    
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _gradient.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:52.0/255.0 green:194.0/255.0 blue:220.0/255.0 alpha:1] CGColor], (id)[[UIColor colorWithRed:0.0/255.0 green:131.0/255.0 blue:116.0/255.0 alpha:1] CGColor],nil];
    [_gradient.layer insertSublayer:gradient atIndex:0];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return menuTtls.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    UILabel*title=(UILabel*)[cell viewWithTag:2];
    title.text=menuTtls[indexPath.row];
    
    UIImageView *imgV = (UIImageView*)[cell viewWithTag:1];
    imgV.image = [UIImage imageNamed:menuImgs[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0){
        [self performSegueWithIdentifier:@"main" sender:self];
    }
    else if (indexPath.row==1){
        [self performSegueWithIdentifier:@"Library" sender:self];
    }
    else if (indexPath.row==2){
        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
        if([user objectForKey:@"email"] !=nil && ![[user objectForKey:@"email"] isEqualToString:@""]){
            [self performSegueWithIdentifier:@"MyBooks" sender:self];
        }
        else {
            [self performSegueWithIdentifier:@"auth" sender:self];
        }
    }
    else if (indexPath.row==3){
        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
        if([user objectForKey:@"email"] !=nil && ![[user objectForKey:@"email"] isEqualToString:@""]){
            [self performSegueWithIdentifier:@"Profile" sender:self];
        }
        else {
            [self performSegueWithIdentifier:@"auth" sender:self];
        }
    }
    else if (indexPath.row==4){
        [self performSegueWithIdentifier:@"contacts" sender:self];
    }
    else if (indexPath.row==5){
        [self performSegueWithIdentifier:@"About" sender:self];
    }else if (indexPath.row==6){
        [self performSegueWithIdentifier:@"Share" sender:self];
    }
}

@end
