//
//  BookRead.h
//  Kokzhiek
//
//  Created by Admin on 16.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookRead : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *wv;
@property NSString *tl;
@property NSURL *path;
@end
