//
//  Profile.h
//  Kokzhiek
//
//  Created by bugingroup on 22.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Profile : UIViewController
@property (strong, nonatomic) IBOutlet UIBarButtonItem *menu;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segment;
- (IBAction)segment:(id)sender;
- (IBAction)stepper:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnFinishOrder;
@property (weak, nonatomic) IBOutlet UILabel *lblSum;
@property (weak, nonatomic) IBOutlet UIView *vSum;

@end
