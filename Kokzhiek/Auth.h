//
//  Auth.h
//  Kokzhiek
//
//  Created by bugingroup on 10.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Auth : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIBarButtonItem *menu;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *password;
- (IBAction)sign_button:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *sign_button;
@property (weak, nonatomic) IBOutlet UIButton *btnRestore;
@property (strong, nonatomic) IBOutlet UIButton *registration;
- (IBAction)registration:(id)sender;
@end
