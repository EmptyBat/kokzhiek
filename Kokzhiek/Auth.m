//
//  Auth.m
//  Kokzhiek
//
//  Created by bugingroup on 10.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "Auth.h"
#import "SWRevealViewController.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "VkontakteDelegate.h"
#import "constants.h"
#import "SocialAuth.h"

@interface Auth (){
    UIWebView *authWebView;
    NSString *socialId;
    NSString *socialProvider;
    NSString *socialName;
    NSString *socialEmail;
}

@end

@implementation Auth

- (void)viewDidLoad {
    [super viewDidLoad];
    _email.delegate=self;
    _password.delegate=self;
    _menu.target = self.revealViewController;
    _menu.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    //self.navigationController.navigationBar.translucent = TRUE;
    
    [self.btnRestore setTitle:NSLocalizedString(@"PASSWORD_RESTORE", @"") forState:(UIControlStateNormal)];
    
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    self.btnRestore.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Восстановление пароля"
                                                             attributes:underlineAttribute];
    
    self.navigationController.navigationBar.translucent = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField==self.email){
        [self.password becomeFirstResponder];
    }
    else   if (theTextField==self.password){
        [theTextField resignFirstResponder];
    }
    else  {
        [theTextField resignFirstResponder];
    }
    return YES;
}
- (IBAction)sign_button:(id)sender {
    if (![_email.text isEqualToString:@""]&&![_password.text isEqualToString:@""]){
        NSString*url=[NSString stringWithFormat:@"http://kokzhiek.kz/kk/mobile/login?email=%@&password=%@",_email.text,_password.text];
        [SVProgressHUD show];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            NSLog(@"**+++%@",responseObject);
            [SVProgressHUD dismiss];
            [_email resignFirstResponder];
            [_password resignFirstResponder];
            if ([responseObject objectForKey:@"result"]) {
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.labelText = NSLocalizedString(@"WRONG_DATA", @"");
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;
                
                [hud hide:YES afterDelay:2];
            }
            else {
                NSString *address = [responseObject objectForKey:@"address"];
                NSString *avatar = [responseObject objectForKey:@"avatar"];
                NSString *name = [responseObject objectForKey:@"name"];
                NSString *phone = [responseObject objectForKey:@"phone"];
                [[NSUserDefaults standardUserDefaults] setObject:_email.text forKey:@"email"];
                [[NSUserDefaults standardUserDefaults] setObject:_password.text forKey:@"password"];
                [[NSUserDefaults standardUserDefaults] setObject:address forKey:@"address"];
                [[NSUserDefaults standardUserDefaults] setObject:avatar forKey:@"avatar"];
                [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"name"];
                [[NSUserDefaults standardUserDefaults] setObject:phone forKey:@"phone"];
                [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"issocial"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"Profile"];
                [[self navigationController] pushViewController:viewController animated:YES];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [SVProgressHUD dismiss];
        }];
        
    }else{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = NSLocalizedString(@"PLEASE_FILL_IN", @"");
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
}
- (IBAction)registration:(id)sender {
    [self performSegueWithIdentifier:@"registration" sender:self];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}
- (IBAction)gmailButton:(id)sender {
    //создаем webView
    authWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    authWebView.tag = 1024;
    authWebView.delegate = self;
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"Chrome/56.0.0.0 Mobile", @"UserAgent", nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
    UIButton* closeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.view addSubview:authWebView];
    [authWebView loadRequest: [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://accounts.google.com/o/oauth2/auth?response_type=token&client_id=8395963463-ks5hhh797eugdgcbml1jrvqtqj10s8jo.apps.googleusercontent.com&redirect_uri=http://kokzhiek.kz/callback/google&scope=email%20profile"]]];
    //обеспечиваем появление клавиатуры для авторизации
    [self.view.window makeKeyAndVisible];
    //создаем кнопочку для закрытия окна авторизации
    closeButton.frame = CGRectMake(self.view.frame.size.width - 12 - 25, 7, 30, 30);
    closeButton.backgroundColor = [UIColor blackColor];
    closeButton.tag = 1025;
    [closeButton addTarget:self action:@selector(closeWebViewButton:) forControlEvents:UIControlEventTouchUpInside];
    [closeButton setTitle:@"X" forState:UIControlStateNormal];
    closeButton.titleLabel.font = [UIFont systemFontOfSize:30];
    [closeButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [self.view addSubview:closeButton];
}

- (IBAction)facebookButton:(id)sender {
    //создаем webView
    authWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    authWebView.tag = 1024;
    authWebView.delegate = self;
    UIButton* closeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.view addSubview:authWebView];
    [authWebView loadRequest: [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://graph.facebook.com/oauth/authorize?client_id=963480303764509&redirect_uri=http://www.facebook.com/connect/login_success.html&scope=public_profile,email&type=user_agent&display=touch"]]];
    //обеспечиваем появление клавиатуры для авторизации
    [self.view.window makeKeyAndVisible];
    //создаем кнопочку для закрытия окна авторизации
    closeButton.frame = CGRectMake(self.view.frame.size.width - 12 - 25, 7, 30, 30);
    closeButton.tag = 1025;
    closeButton.backgroundColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(closeWebViewButton:) forControlEvents:UIControlEventTouchUpInside];
    [closeButton setTitle:@"X" forState:UIControlStateNormal];
    closeButton.titleLabel.font = [UIFont systemFontOfSize:30];
    [closeButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [self.view addSubview:closeButton];
}

-(IBAction)vkontakteButton:(id)sender {
    //создаем webView
    authWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    authWebView.tag = 1024;
    authWebView.delegate = self;
    UIButton* closeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.view addSubview:authWebView];
    [authWebView loadRequest: [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://oauth.vk.com/authorize?client_id=5500199&scope=wall,offline,email&redirect_uri=oauth.vk.com/blank.html&display=touch&response_type=token"]]];
    //обеспечиваем появление клавиатуры для авторизации
    [self.view.window makeKeyAndVisible];
    //создаем кнопочку для закрытия окна авторизации
    closeButton.frame = CGRectMake(self.view.frame.size.width - 12 - 25, 7, 30, 30);
    closeButton.tag = 1025;
    closeButton.backgroundColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(closeWebViewButton:) forControlEvents:UIControlEventTouchUpInside];
    [closeButton setTitle:@"X" forState:UIControlStateNormal];
    closeButton.titleLabel.font = [UIFont systemFontOfSize:30];
    [closeButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [self.view addSubview:closeButton];
}
-(void) closeWebView {
    [[self.view viewWithTag:1024] removeFromSuperview];
    [[self.view viewWithTag:1025] removeFromSuperview];
}
-(IBAction)closeWebViewButton:(id)sender {
    [self closeWebView];
}

-(void) webViewDidFinishLoad:(UIWebView *)webView {
    //создадим хеш-таблицу для хранения данных
    NSMutableDictionary* user = [[NSMutableDictionary alloc] init];
    //смотрим на адрес открытой станицы
    NSString *currentURL = webView.request.URL.absoluteString;
    NSRange textRange =[[currentURL lowercaseString] rangeOfString:[@"access_token" lowercaseString]];
    
    //смотрим, содержится ли в адресе информация о токене
    
    NSLog(@"\nURL: %@", currentURL);
    
    if ([currentURL rangeOfString:@"vk.com"].location != NSNotFound) {
        if(textRange.location != NSNotFound){
            NSArray* data = [currentURL componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"=&"]];
            [user setObject:[data objectAtIndex:1] forKey:@"access_token"];
            [user setObject:[data objectAtIndex:3] forKey:@"expires_in"];
            [user setObject:[data objectAtIndex:5] forKey:@"user_id"];
            [self closeWebView];
            //передаем всю информацию специально обученному классу
            //[[VkontakteDelegate sharedInstance] loginWithParams:user];
            
            NSString*ID = [user objectForKey:@"user_id"];
            //NSString*access_token = [user objectForKey:@"access_token"];
            
            self->socialId = ID;
            self->socialProvider = @"Provider";
            self->socialEmail = @"";
            self->socialName = @"";
            
            [self go];
            
            //            //Сохраняемся, ребят!
            //            [[NSUserDefaults standardUserDefaults] setValue:access_token forKey:@"vk_token"];
            //            [[NSUserDefaults standardUserDefaults] setValue:ID forKey:@"vk_id"];
            //            [[NSUserDefaults standardUserDefaults] synchronize];
            //            //Теперь попробуем вытяныть некую информацию
            //            NSString *urlString = [NSString stringWithFormat:@"https://api.vk.com/method/users.get?uid=%@&access_token=%@", ID, access_token] ;
            //            NSURL *url = [NSURL URLWithString:urlString];
            //            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            //            NSHTTPURLResponse *response = nil;
            //            NSError *error = nil;
            //            NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            //
            //            NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
            //
            //            //Тут произошла странная вещь - ответ должен вернуться в словаре, ан нет!
            //            //У меня не получилось пропарсить стандартными средствами.
            //            //Строим костыли, простите...
            //            NSArray* userData = [responseString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\":{},[]"]];
            //            NSString *realName = [userData objectAtIndex:14];
            //            realName = [realName stringByAppendingString:@" "];
            //            realName = [realName stringByAppendingString:[userData objectAtIndex:20]];
            //
            //            //Кому надо, сохраняемся
            //            [[NSUserDefaults standardUserDefaults] setValue:@"vkontakte" forKey:@"SignedUpWith"];
            //            [[NSUserDefaults standardUserDefaults] setValue:realName forKey:@"RealUsername"];
            //            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
        }
        else {
            textRange =[[currentURL lowercaseString] rangeOfString:[@"access_denied" lowercaseString]];
            if (textRange.location != NSNotFound) {
                //                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ooops! something gonna wrong..." message:@"Check your internet connection and try again!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                //                [alert show];
                [self closeWebView];
            }
            return;
        }
    } else if ([currentURL rangeOfString:@"facebook.com"].location != NSNotFound){
        
        
        if ([currentURL rangeOfString:@"error_reason=user_denied"].location != NSNotFound || [currentURL rangeOfString:@"error=access_denied"].location != NSNotFound){
            [self closeWebView];
            return;
        }
        
        
        if(textRange.location != NSNotFound){
            NSArray* data = [currentURL componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"=&"]];
            [user setObject:[data objectAtIndex:1] forKey:@"access_token"];
            
            NSString*access_token = [user objectForKey:@"access_token"];
            [self closeWebView];
            
            NSString*url2=[NSString stringWithFormat:@"https://graph.facebook.com/v2.6/me?fields=id,name,last_name,first_name,email&access_token=%@",access_token];
            NSLog(@"%@",url2);
            url2 = [url2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            [manager GET:url2 parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                
                self->socialId = [responseObject objectForKey:@"id"];
                self->socialProvider = @"FacebookProvider";
                self->socialEmail = [responseObject objectForKey:@"email"];
                
                NSString *fn = [responseObject objectForKey:@"first_name"];
                NSString *ln = [responseObject objectForKey:@"last_name"];
                
                if(fn == nil){
                    fn = @"";
                }
                if(ln == nil){
                    ln = @"";
                }
                
                self->socialName = [NSString stringWithFormat:@"%@ %@",ln,fn];
                if([self->socialName isEqualToString:@" "]){
                    self->socialName = [responseObject objectForKey:@"name"];
                }
                
                [self go];
            } failure:^(NSURLSessionTask *operation, NSError *error) {
                NSLog(@"Error: %@", error);
            }];
        }
        //        else {
        //            //Ну иначе сообщаем об ошибке...
        //            textRange =[[currentURL lowercaseString] rangeOfString:[@"access_denied" lowercaseString]];
        //            if (textRange.location != NSNotFound) {
        ////                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ooops! something gonna wrong..." message:@"Check your internet connection and try again!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        ////                [alert show];
        //                [self closeWebView];
        //            }
        //            return;
        //        }
        
    }else if ([currentURL rangeOfString:@"http://kokzhiek.kz/callback/google"].location != NSNotFound){
        if(textRange.location != NSNotFound){
            NSArray* data = [currentURL componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"=&"]];
            [user setObject:[data objectAtIndex:1] forKey:@"access_token"];
            
            NSString*access_token = [user objectForKey:@"access_token"];
            [self closeWebView];
            
            NSString*url2=[NSString stringWithFormat:@"https://www.googleapis.com/oauth2/v1/userinfo?access_token=%@",access_token];
            NSLog(@"%@",url2);
            url2 = [url2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            [manager GET:url2 parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                
                self->socialId = [responseObject objectForKey:@"id"];
                self->socialProvider = @"GoogleProvider";
                self->socialEmail = [responseObject objectForKey:@"email"];
                
                NSString *fn = [responseObject objectForKey:@"given_name"];
                NSString *ln = [responseObject objectForKey:@"family_name"];
                
                if(fn == nil){
                    fn = @"";
                }
                if(ln == nil){
                    ln = @"";
                }
                
                self->socialName = [NSString stringWithFormat:@"%@ %@",ln,fn];
                if([self->socialName isEqualToString:@" "]){
                    self->socialName = [responseObject objectForKey:@"nickname"];
                }
                
                [self go];
            } failure:^(NSURLSessionTask *operation, NSError *error) {
                NSLog(@"Error: %@", error);
            }];
        }
    }
    
    
    
}

-(void)go{
    NSString*url2=[NSString stringWithFormat:@"%@kk/mobile/socialcheck?provider_user_id=%@&provider=%@",kBaseURL,self->socialId, self->socialProvider];
    NSLog(@"%@",url2);
    [SVProgressHUD show];
    url2 = [url2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url2 parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        [SVProgressHUD dismiss];
        
        if([[responseObject objectForKey:@"result"] isKindOfClass:[NSString class]]
           &&
           [[responseObject objectForKey:@"result"] isEqualToString:@"noemail"]){
            [self performSegueWithIdentifier:@"socialauth" sender:self];
        }
        else{
            
            NSDictionary *dic =[ responseObject objectForKey:@"result"];
            NSString *address = [dic objectForKey:@"address"];
            NSString *avatar = [dic objectForKey:@"avatar"];
            NSString *name = [dic objectForKey:@"name"];
            NSString *phone = [dic objectForKey:@"phone"];
            NSString *email = [dic objectForKey:@"email"];
            
            [[NSUserDefaults standardUserDefaults] setObject:email forKey:@"email"];
            [[NSUserDefaults standardUserDefaults] setObject:@"kokzhiek062016" forKey:@"password"];
            [[NSUserDefaults standardUserDefaults] setObject:address forKey:@"address"];
            [[NSUserDefaults standardUserDefaults] setObject:avatar forKey:@"avatar"];
            [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"name"];
            [[NSUserDefaults standardUserDefaults] setObject:phone forKey:@"phone"];
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"issocial"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"Profile"];
            [[self navigationController] pushViewController:viewController animated:YES];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [SVProgressHUD dismiss];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"socialauth"]) {
        
        SocialAuth *vc=segue.destinationViewController;
        vc.Id = self->socialId;
        vc.Provider = self->socialProvider;
        vc.Email = self->socialEmail;
        vc.Name = self->socialName;
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }
}
- (IBAction)btnRestoreAction:(id)sender {
}
@end
