//
//  RestorePassword.h
//  Kokzhiek
//
//  Created by Admin on 09.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestorePassword : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;

@end
