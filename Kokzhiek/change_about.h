//
//  change_about.h
//  Kokzhiek
//
//  Created by bugingroup on 23.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface change_about : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *name;
@property (strong, nonatomic) IBOutlet UITextField *address;
@property (strong, nonatomic) IBOutlet UITextField *phone;
@property (strong, nonatomic) IBOutlet UIButton *done;
- (IBAction)done:(id)sender;
@end
