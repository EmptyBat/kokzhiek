//
//  Gift.h
//  Kokzhiek
//
//  Created by Admin on 09.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BMEpayViewController.h"

@interface Gift : UIViewController<UITextFieldDelegate,BMEpayControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property NSString *bookId;
@property NSString *price;
@end
