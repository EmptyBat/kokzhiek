//
//  PaymentType.m
//  Kokzhiek
//
//  Created by Admin on 05.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "PaymentType.h"
#import "AFNetworking.h"
#import <TLYShyNavBar/TLYShyNavBarManager.h>
#import "SWRevealViewController.h"
#import "SVProgressHUD.h"
#import "constants.h"
#import "BookDetail.h"
#import "MBProgressHUD.h"
#import "FTWCache.h"
#import "NSString+MD5.h"
#import "BMEpayViewController.h"

@interface PaymentType ()

@end

@implementation PaymentType

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIImage *img = [UIImage imageNamed:@"fon"];
    UIImageView *imgV = [[UIImageView alloc] initWithImage:img];
    imgV.contentMode = UIViewContentModeScaleAspectFill;
    self.tableView.backgroundView = imgV;
    
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.layoutMargins = UIEdgeInsetsZero;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *simpleTableIdentifier = @"";
    
    if(indexPath.row == 0){
        simpleTableIdentifier = @"cell1";
    }else{
        simpleTableIdentifier = @"cell2";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    cell.layoutMargins = UIEdgeInsetsZero;
    cell.preservesSuperviewLayoutMargins = false;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    NSString*password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
    
    NSString*url =@"";
    
    if(indexPath.row == 0){
        if([self.deliveryType isEqualToString:@"courier"]){
            
            NSString*u=[NSString stringWithFormat:@"%@kk/mobile/mybasket?email=%@&password=%@",kBaseURL,email,password];
            
            [SVProgressHUD show];
            u = [u stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            [manager GET:u parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                NSLog(@"**+++%@",responseObject);
                
                NSInteger pr = [[responseObject objectForKey:@"total_price"] intValue];
                NSString*url = @"";
                
                if(pr >= 5000){
                    url=[NSString stringWithFormat:@"%@kk/mobile/confirmorder?email=%@&password=%@&customer_name=%@&customer_email=%@&customer_phone=%@&address=%@&delivery=courier&payment_type=epay&courier_adress=%@&courier_street=%@&courier_home=%@&courier_office=%@&courier_access=%@",
                         kBaseURL,email,password,self.txtFio,self.txtEmail,self.txtPhone,self.txtCity,self.txtNaselPunkt,self.txtStreet,self.txtNDom,self.txtNKv,self.txtNPodiesd];
                }else{
                    url=[NSString stringWithFormat:@"%@kk/mobile/confirmorder?email=%@&password=%@&customer_name=%@&customer_email=%@&customer_phone=%@&address=%@&delivery=courier&payment_type=epay&courier_adress=%@&courier_street=%@&courier_home=%@&courier_office=%@&courier_access=%@&delivery_cost=700",
                         kBaseURL,email,password,self.txtFio,self.txtEmail,self.txtPhone,self.txtCity,self.txtNaselPunkt,self.txtStreet,self.txtNDom,self.txtNKv,self.txtNPodiesd];
                }
                
                [self order:url row:indexPath.row];
                
            } failure:^(NSURLSessionTask *operation, NSError *error) {
                NSLog(@"Error: %@", error);
            }];
                
            
        }else if([self.deliveryType isEqualToString:@"kazpost"]){
            url=[NSString stringWithFormat:@"%@kk/mobile/confirmorder?email=%@&password=%@&customer_name=%@&customer_email=%@&customer_phone=%@&address=%@&delivery=kazpost&payment_type=epay&kazpost_name=%@&kazpost_postcode=%@&kazpost_street=%@&kazpost_house=%@&kazpost_flat=%@&kazpost_city=%@&kazpost_district=%@",
                 kBaseURL,email,password,self.txtFio,self.txtEmail,self.txtPhone,self.txtCity,self.txtAdresat,self.txtIndex,self.txtStreet,self.txtNDom,self.txtNKv, self.txtNaselPunkt, self.txtDistrict];
            
            [self order:url row:indexPath.row];
        }else{
            url=[NSString stringWithFormat:@"%@kk/mobile/confirmorder?email=%@&password=%@&customer_name=%@&customer_email=%@&customer_phone=%@&address=%@&delivery=pickup&payment_type=epay",
                 kBaseURL,email,password,self.txtFio,self.txtEmail,self.txtPhone,self.txtCity];
            
            [self order:url row:indexPath.row];
        }
    }else{
        if([self.deliveryType isEqualToString:@"courier"]){
            
            NSString*u=[NSString stringWithFormat:@"%@kk/mobile/mybasket?email=%@&password=%@",kBaseURL,email,password];
            
            [SVProgressHUD show];
            u = [u stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            [manager GET:u parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                NSLog(@"**+++%@",responseObject);
                
                NSInteger pr = [[responseObject objectForKey:@"total_price"] intValue];
                NSString*url = @"";
                
                if(pr >= 5000){
                    url=[NSString stringWithFormat:@"%@kk/mobile/confirmorder?email=%@&password=%@&customer_name=%@&customer_email=%@&customer_phone=%@&address=%@&delivery=courier&payment_type=receiving&courier_adress=%@&courier_street=%@&courier_home=%@&courier_office=%@&courier_access=%@",
                         kBaseURL,email,password,self.txtFio,self.txtEmail,self.txtPhone,self.txtCity,self.txtNaselPunkt,self.txtStreet,self.txtNDom,self.txtNKv,self.txtNPodiesd];
                }else{
                    url=[NSString stringWithFormat:@"%@kk/mobile/confirmorder?email=%@&password=%@&customer_name=%@&customer_email=%@&customer_phone=%@&address=%@&delivery=courier&payment_type=receiving&courier_adress=%@&courier_street=%@&courier_home=%@&courier_office=%@&courier_access=%@&delivery_cost=700",
                         kBaseURL,email,password,self.txtFio,self.txtEmail,self.txtPhone,self.txtCity,self.txtNaselPunkt,self.txtStreet,self.txtNDom,self.txtNKv,self.txtNPodiesd];
                }
                
                [self order:url row:indexPath.row];
                
            } failure:^(NSURLSessionTask *operation, NSError *error) {
                NSLog(@"Error: %@", error);
            }];
            
            
            
        }else if([self.deliveryType isEqualToString:@"kazpost"]){
            url=[NSString stringWithFormat:@"%@kk/mobile/confirmorder?email=%@&password=%@&customer_name=%@&customer_email=%@&customer_phone=%@&address=%@&delivery=kazpost&payment_type=receiving&kazpost_name=%@&kazpost_postcode=%@&kazpost_street=%@&kazpost_house=%@&kazpost_flat=%@&kazpost_city=%@&kazpost_district=%@",
                 kBaseURL,email,password,self.txtFio,self.txtEmail,self.txtPhone,self.txtCity,self.txtAdresat,self.txtIndex,self.txtStreet,self.txtNDom,self.txtNKv, self.txtNaselPunkt, self.txtDistrict];
            
            [self order:url row:indexPath.row];
        }else{
            url=[NSString stringWithFormat:@"%@kk/mobile/confirmorder?email=%@&password=%@&customer_name=%@&customer_email=%@&customer_phone=%@&address=%@&delivery=pickup&payment_type=receiving",
                 kBaseURL,email,password,self.txtFio,self.txtEmail,self.txtPhone,self.txtCity];
            
            [self order:url row:indexPath.row];
        }
    }
}

-(void)order:(NSString*)url row:(NSInteger)row{
    NSLog(@"%@",url);
    [SVProgressHUD show];
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"DATA: %@", responseObject);
        [SVProgressHUD dismiss];
        if(row == 1){
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.labelText = NSLocalizedString(@"ORDER_SENT", @"");
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.removeFromSuperViewOnHide = YES;
            
            [hud hide:YES afterDelay:2];
            [self.navigationController popToRootViewControllerAnimated:true];
        }else{
            NSString *b64 = [responseObject valueForKey:@"base64"];
            
            BMEpayViewController *controller = [[BMEpayViewController alloc] init];
            controller.delegate = self;
            controller.sdkMode = EpaySdkModeProduction;
            controller.signedOrderBase64 = b64;
            controller.postLink = @"http://kokzhiek.kz/user/test";
            //controller.template = @"besmart_android.xsl";
            controller.clientEmail = self.txtEmail;
            controller.language = EpayLanguageRussian;
            
            [self presentViewController:controller animated:YES completion:^{
                
            }];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [SVProgressHUD dismiss];
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = NSLocalizedString(@"ERROR", @"");
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        
        [hud hide:YES afterDelay:2];
    }];
}

-(void)epayPaymentSuccededWithController:(BMEpayViewController*)epayViewController {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = NSLocalizedString(@"PAYED", @"");
    hud.margin = 10.f;
    hud.yOffset = 150.f;
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hide:YES afterDelay:2];
    [self.navigationController popToRootViewControllerAnimated:true];
    NSLog(@"SUCCESS");
}

-(void)epayPaymentFailedWithController:(BMEpayViewController*)epayViewController {
    NSLog(@"FAIL");
}

-(void)epayPaymentCancelledWithController:(BMEpayViewController*)epayViewController {
    NSLog(@"CANCEL");
}
@end
