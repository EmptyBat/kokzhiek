//
//  SideBar.h
//  Kokzhiek
//
//  Created by bugingroup on 03.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "ViewController.h"

@interface SideBar : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UIView *gradient;

@end
