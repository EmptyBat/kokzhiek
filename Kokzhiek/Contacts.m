//
//  Contacts.m
//  Kokzhiek
//
//  Created by Admin on 03.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "Contacts.h"
#import "SWRevealViewController.h"

@interface Contacts ()

@end

@implementation Contacts

- (void)viewDidLoad {
    [super viewDidLoad];
    _menu.target = self.revealViewController;
    _menu.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    self.txtAddress.text = NSLocalizedString(@"CONTACTS_ADDRESS", @"");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onMapAction:(id)sender {
    
}
- (IBAction)contactAction:(id)sender {
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://kokzhiek.kz/ru/page/information"]];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
