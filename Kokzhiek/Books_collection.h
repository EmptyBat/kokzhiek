//
//  Books_collection.h
//  Kokzhiek
//
//  Created by bugingroup on 14.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Books_collection : UIViewController<UIActionSheetDelegate, UISearchBarDelegate>
@property (strong, nonatomic) IBOutlet UIBarButtonItem *menu;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
