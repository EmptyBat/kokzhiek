//
//  Demo.h
//  Kokzhiek
//
//  Created by Admin on 08.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Demo : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *web;
@property NSURL *url;
@end
