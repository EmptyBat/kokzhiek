//
//  Library.m
//  Kokzhiek
//
//  Created by bugingroup on 07.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "Library.h"
#import "Library-1.h"
#import "Library-2.h"
#import "Library-3.h"
#import "Library-4.h"
#import "Library-5.h"
#import "Library-6.h"
#import "BookDetail.h"

@interface Library ()
{
    NSDictionary*BookDetails;
}
@end

@implementation Library

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    
    
    [self.swipeBar.selectedBar setBackgroundColor:[UIColor orangeColor]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(BookDetail:)
                                                 name:@"BookDetail" object:nil];
}

- (void)BookDetail:(NSNotification *)notice{
    BookDetails = [notice object];
    [self performSegueWithIdentifier:@"BookDetail" sender:self];
}

#pragma mark - XLSwipeContainerControllerDataSource

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"BookDetail"]) {
        BookDetail*Detail=segue.destinationViewController;
        Detail.image_url = [BookDetails objectForKey:@"image"];
        Detail.book_title = [BookDetails objectForKey:@"title"];
        Detail.author = [BookDetails objectForKey:@"author"];
        Detail.isbn = [BookDetails objectForKey:@"isbn"];
        Detail.description_book = [BookDetails objectForKey:@"description"];
        Detail.ebook_price = [BookDetails objectForKey:@"ebook_price"];
        Detail.price = [BookDetails objectForKey:@"price"];
        Detail.page = [BookDetails objectForKey:@"page"];
        Detail.publish_year = [BookDetails objectForKey:@"publish_year"];
        Detail.formats = [BookDetails objectForKey:@"formats"];
        Detail.status = [BookDetails objectForKey:@"status"];
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
    }
}
-(NSArray *)swipeContainerControllerViewControllers:(XLSwipeContainerController *)swipeContainerController

{
    
    // create child view controllers that will be managed by XLSwipeContainerController
    
    UICollectionViewFlowLayout *aFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    [aFlowLayout setItemSize:CGSizeMake(142, 210)]; [aFlowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    
    
    Library_1*child_1 = [[Library_1 alloc]initWithCollectionViewLayout:aFlowLayout];
    
    Library_2*child_2 = [[Library_2 alloc]initWithCollectionViewLayout:aFlowLayout];
    
    Library_3*child_3 = [[Library_3 alloc]initWithCollectionViewLayout:aFlowLayout];
    
    Library_4*child_4 = [[Library_4 alloc]initWithCollectionViewLayout:aFlowLayout];
    
    Library_5*child_5 = [[Library_5 alloc]initWithCollectionViewLayout:aFlowLayout];
    
    Library_6*child_6 = [[Library_6 alloc]initWithCollectionViewLayout:aFlowLayout];
    
    return @[child_1, child_2,child_3,child_4,child_5,child_6];
    
}


@end
