//
//  KazPost.m
//  Kokzhiek
//
//  Created by Admin on 05.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "KazPost.h"
#import "PaymentType.h"
#import "MBProgressHUD.h"

@interface KazPost ()

@end

@implementation KazPost

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.txtAdresat.delegate = self;
    self.txtIndex.delegate = self;
    self.txtStreet.delegate = self;
    self.txtNDom.delegate = self;
    self.txtNKv.delegate = self;
    self.txtNaselPunkt.delegate = self;
    self.txtDistrict.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField==self.txtAdresat){
        [self.txtIndex becomeFirstResponder];
    }
    else   if (theTextField==self.txtIndex){
        [self.txtStreet becomeFirstResponder];
    }
    else   if (theTextField==self.txtStreet){
        [self.txtNDom becomeFirstResponder];
    }
    else   if (theTextField==self.txtNDom){
        [self.txtNKv becomeFirstResponder];
    }
    else   if (theTextField==self.txtNKv){
        [self.txtNaselPunkt becomeFirstResponder];
    }
    else   if (theTextField==self.txtNaselPunkt){
        [self.txtDistrict becomeFirstResponder];
    }
    else  {
        [theTextField resignFirstResponder];
    }
    return YES;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    Boolean b = YES;
    
    if([identifier isEqualToString:@"payment"]){
        if([self.txtAdresat.text isEqualToString:@""]){
            b = NO;
        }
        if([self.txtIndex.text isEqualToString:@""]){
            b = NO;
        }
        if([self.txtStreet.text isEqualToString:@""]){
            b = NO;
        }
        if([self.txtNDom.text isEqualToString:@""]){
            b = NO;
        }
        if([self.txtNKv.text isEqualToString:@""]){
            b = NO;
        }
        if([self.txtNaselPunkt.text isEqualToString:@""]){
            b = NO;
        }
        if([self.txtDistrict.text isEqualToString:@""]){
            b = NO;
        }
    }
    
    if(b == NO){
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = NSLocalizedString(@"PLEASE_FILL_IN", @"");
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        
        [hud hide:YES afterDelay:2];
    }
    
    return b;
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"payment"]) {
        
        PaymentType *vc=segue.destinationViewController;
        vc.txtFio = self.txtFio;
        vc.txtEmail = self.txtEmail;
        vc.txtPhone = self.txtPhone;
        vc.txtCity = self.txtCity;
        
        vc.deliveryType = @"kazpost";
        
        vc.txtAdresat = self.txtAdresat.text;
        vc.txtIndex = self.txtIndex.text;
        vc.txtStreet = self.txtStreet.text;
        vc.txtNDom = self.txtNDom.text;
        vc.txtNKv = self.txtNKv.text;
        vc.txtNaselPunkt = self.txtNaselPunkt.text;
        vc.txtDistrict = self.txtDistrict.text;
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    }
}
@end
