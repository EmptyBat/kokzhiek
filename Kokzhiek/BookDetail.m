//
//  BookDetail.m
//  Kokzhiek
//
//  Created by bugingroup on 03.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "BookDetail.h"
#import "FTWCache.h"
#import "NSString+MD5.h"
#import "constants.h"
#import "about_of_book.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "Comments.h"
#import "Gift.h"
#import <TLYShyNavBar/TLYShyNavBarManager.h>
#import "ORGContainerCellView.h"
#import "ORGContainerCell.h"
#import "ORGArticleCollectionViewCell.h"
#import "EPubViewController.h"
#import "AppDelegate.h"
#import "Demo.h"
#import "MMAlertView.h"
#import "MMSheetView.h"
@import FolioReaderKit;
@interface BookDetail ()<MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
    NSData *imgData;
    Boolean showReadButton;
    
}
@end

@implementation BookDetail


- (void)viewDidLoad {
    [super viewDidLoad];
    //self.title=_book_title;
    self.title = NSLocalizedString(@"FULL_INFO", @"");
    
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.layoutMargins = UIEdgeInsetsZero;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 50;
    
    if ([self.formats rangeOfString:@"epub"].location == NSNotFound) {
        self.btnBookBuy1.hidden = YES;
        self.btnBookBuy2.hidden = YES;
        [self.btnBookBuy3 setTitle:[NSString stringWithFormat:@"%@ тг", self.price] forState:(UIControlStateNormal)];
    } else {
        [self.btnBookBuy1 setTitle:[NSString stringWithFormat:@"%@ тг", self.price] forState:(UIControlStateNormal)];
        [self.btnBookBuy2 setTitle:[NSString stringWithFormat:@"%@ тг, (epub)", self.ebook_price] forState:(UIControlStateNormal)];
        self.btnBookBuy3.hidden = YES;
    }
    
    if(![self.status isEqualToString:@"Қоймада бар"]){
        self.btnBookBuy1.hidden = YES;
        self.btnBookBuy2.hidden = YES;
        self.btnBookBuy3.hidden = NO;
        self.btnBookBuy3.enabled = NO;
        self.btnBookBuy3.backgroundColor = [UIColor grayColor];
        [self.btnBookBuy3 setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [self.btnBookBuy3 setTitle:NSLocalizedString(@"NO_BOOK", @"") forState:(UIControlStateNormal)];
    }
    
    self->showReadButton = NO;
    
    if([self.source isEqualToString:@""]){
        
    }else{
        NSString*url=[NSString stringWithFormat:@"%@ru/mobile/read-demo/%@",kBaseURL, self.book_id];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            showReadButton = YES;
            [self.tableView reloadData];
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            showReadButton = NO;
            //[self.tableView reloadData];
        }];
    }
    
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Books"];
    self.devices = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
}
-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self->showReadButton){
        return 5;
    }else{
        return 4;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.row==1){
//        [self performSegueWithIdentifier:@"about" sender:self];
//    }
//    else if (indexPath.row==2){
//        [self performSegueWithIdentifier:@"option" sender:self];
//    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger r = indexPath.row;
    
    if (r==0){
        static NSString *cellIdentifier = @"cell1";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        
        UITextView*title=(UITextView*)[cell viewWithTag:2];
        UILabel*author=(UILabel*)[cell viewWithTag:3];
        UIImageView*image_view=(UIImageView*)[cell viewWithTag:1];
        title.text=[NSString stringWithFormat:@"%@",_book_title];
        
        if(_author == nil || [_author  isEqual: @""]){
            _author = NSLocalizedString(@"NOT_SET", "");
        }
        author.text= [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"BOOK_AUTHOR", ""), _author ];
        NSString*imagename=[NSString stringWithFormat:@"%@%@",KimageURL,_image_url];
        imagename = [imagename stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSURL *imageURL = [NSURL URLWithString:imagename];
        
        
        
        NSString *key = [imagename MD5Hash];
        imgData = [FTWCache objectForKey:key];
        if (imgData) {
            
            UIImage *image = [UIImage imageWithData:imgData];
            image_view.image = image;
            image_view.contentMode=UIViewContentModeScaleAspectFill;
            [image_view setClipsToBounds:YES];
            
            
        } else {
            image_view.image = [UIImage imageNamed:@"icon.png"];
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
            dispatch_async(queue, ^{
                NSData *data = [NSData dataWithContentsOfURL:imageURL];
                [FTWCache setObject:data forKey:key];
                UIImage *image = [UIImage imageWithData:data];
                dispatch_sync(dispatch_get_main_queue(), ^{
                    image_view.image = image;
                    image_view.contentMode=UIViewContentModeScaleAspectFill;
                    [image_view setClipsToBounds:YES];
                });
                
            });
        }
        
        return cell;
    }
    
    if(self->showReadButton){
        
    }else{
        r = r + 1;
    }
    
    if (r==1){
        static NSString *cellIdentifier = @"cell2";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        if(self->showReadButton == NO){
            cell.hidden = YES;
        }
        
        UIButton *b = (UIButton*)[cell.contentView viewWithTag:1];
        [b setTitle:NSLocalizedString(@"BOOK_READ_PART", @"") forState:(UIControlStateNormal)];
        
        return cell;
    }
    if (r==2){
        static NSString *cellIdentifier = @"cell3";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        
        UILabel*lbl=(UILabel*)[cell viewWithTag:1];
        lbl.text=[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"PAGES_COUNT", @""), self.page];
        
        UILabel*lbl2=(UILabel*)[cell viewWithTag:2];
        lbl2.text=[NSString stringWithFormat:@"%@: %@", @"ISBN", self.isbn];
        
        UILabel*lbl3=(UILabel*)[cell viewWithTag:3];
        lbl3.text=[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"PUBLISH_YEAR", @""), self.publish_year];
        
        return cell;
    }
    else if (r==3){
        static NSString *cellIdentifier = @"cell4";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        
        UITextView*txtV=(UITextView*)[cell viewWithTag:1];
        txtV.text = self.description_book;
       
        return cell;
    }
    else {
        static NSString *cellIdentifier = @"cell5";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        
        return cell;
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"about"]) {
        about_of_book*book=segue.destinationViewController;
        book.about =_description_book;
    }
    else if([segue.identifier isEqualToString:@"comments"]) {
        Comments*vc = segue.destinationViewController;
        vc.book_id = self.book_id;
    }
    else if([segue.identifier isEqualToString:@"gift"]) {
        Gift*vc = segue.destinationViewController;
        vc.bookId = self.book_id;
        vc.price = self.ebook_price;
    }
    else if([segue.identifier isEqualToString:@"read"]) {
        
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
}

- (IBAction)save:(id)sender {
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if([user objectForKey:@"email"] !=nil && ![[user objectForKey:@"email"] isEqualToString:@""]){
        NSString*email=[[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
        NSString*password=[[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
        
        NSString*url=[NSString stringWithFormat:@"%@/kk/mobile/book-save?email=%@&password=%@&book_id=%@",kBaseURL,email,password,_book_id];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [SVProgressHUD show];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [SVProgressHUD dismiss];
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.labelText = NSLocalizedString(@"SAVED", @"");
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.removeFromSuperViewOnHide = YES;
            
            [hud hide:YES afterDelay:2];
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [SVProgressHUD dismiss];
        }];
    }
    else {
        /*MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = NSLocalizedString(@"NEED_AUTH", @"");
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        
        [hud hide:YES afterDelay:3];*/
       [self performSegueWithIdentifier:@"sign" sender:self];
    }
}

- (IBAction)commentsAction:(id)sender {
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if([user objectForKey:@"email"] !=nil && ![[user objectForKey:@"email"] isEqualToString:@""]){
      [self performSegueWithIdentifier:@"comment" sender:self];
    }
    else {
      [self performSegueWithIdentifier:@"sign" sender:self];
    }
}
- (IBAction)readAction:(id)sender {
//    BookDetails = [notice object];
//    AppDelegate *appDelegate =
//    [[UIApplication sharedApplication] delegate];
//    NSManagedObjectContext *context =
//    [appDelegate managedObjectContext];
//    NSEntityDescription *entityDesc =
//    [NSEntityDescription entityForName:@"Books"
//                inManagedObjectContext:context];
    
//    NSEntityDescription *entityDesc =
//    [NSEntityDescription entityForName:@"Books"
//                inManagedObjectContext:context];
//    NSFetchRequest *request = [[NSFetchRequest alloc] init];
//    [request setEntity:entityDesc];
//    NSPredicate *pred =
//    [NSPredicate predicateWithFormat:@"(book_id = %@)",[BookDetails objectForKey:@"id"]];
//    [request setPredicate:pred];
//    NSManagedObject *matches = nil;
//    NSError *error;
//    NSArray *objects = [context executeFetchRequest:request
//                                              error:&error];
    
    if(_demo == 0){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        Demo *vc = [storyboard instantiateViewControllerWithIdentifier:@"Demo"];
        
        NSString*combine=[NSString stringWithFormat:@"http://kokzhiek.kz/read_demo/%@",self.book_id];
        NSLog(@"++++9***%@",combine);
        NSURL *w = [NSURL URLWithString:combine];
        vc.url = w;
        
        [[self navigationController] pushViewController:vc animated:YES];
    }else{
        AppDelegate *appDelegate =
        [[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context =
        [appDelegate managedObjectContext];
        NSEntityDescription *entityDesc =
        [NSEntityDescription entityForName:@"Books"
                    inManagedObjectContext:context];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDesc];
        NSPredicate *pred =
        [NSPredicate predicateWithFormat:@"(book_id = %@)",self.book_id];
        [request setPredicate:pred];
        NSManagedObject *matches = nil;
        NSError *error;
        NSArray *objects = [context executeFetchRequest:request
                                                  error:&error];
        if ([objects count] == 0) {
            HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
            [self.navigationController.view addSubview:HUD];
            NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
            AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
            NSString*combine=[NSString stringWithFormat:@"http://www.kokzhiek.kz/books/%@",self.source];
            combine = [combine stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSLog(@"++++9***%@",combine);
            NSURL *URL = [NSURL URLWithString:combine];
            //URL = [NSURL URLWithString:@"http://www.kokzhiek.kz/books/1465286576-demo-maturidi_tandauly_frgm.epub"];
            NSLog(@"***++%@",URL);
            NSURLRequest *request = [NSURLRequest requestWithURL:URL];
            NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
                NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
                NSString*book_id=[NSString stringWithFormat:@"%@",self.book_id];
                return [documentsDirectoryURL URLByAppendingPathComponent:book_id];
            } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
                AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                NSString *urlString = [filePath absoluteString];
                NSManagedObjectContext *context = [appDelegate managedObjectContext];
                NSManagedObject *newContact;
                newContact = [NSEntityDescription insertNewObjectForEntityForName:@"Books" inManagedObjectContext:context];
                NSString*book_id=[NSString stringWithFormat:@"%@",self.book_id];
                //        NSString *img = self.image_url;
                //        NSString*imagename=[NSString stringWithFormat:@"%@img/book_image/%@",kBaseURL,img];
                //        NSURL *imageURL = [NSURL URLWithString:imagename];
                NSString*imagename=[NSString stringWithFormat:@"%@%@",KimageURL,_image_url];
                NSURL *imageURL = [NSURL URLWithString:imagename];
                NSLog(@"***++%@",imageURL);
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [newContact setValue:imageData forKey:@"image"];
                    });
                });
                [newContact setValue:book_id forKey:@"book_id"];
                [newContact setValue:urlString forKey:@"direct"];
                [context save:&error];
                [[NSUserDefaults standardUserDefaults] setObject:filePath forKey:@"book_url"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                NSLog(@"File downloaded to: %@", filePath);
                [[NSUserDefaults standardUserDefaults] setObject:urlString forKey:@"book_url"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                NSString * storyboardName = @"Main";
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
                UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"READER"];
                [self presentViewController:vc animated:YES completion:nil];
                
                
            }];
            [downloadTask resume];
            HUD.mode = MBProgressHUDModeDeterminateHorizontalBar;
            HUD.delegate = self;
            [HUD showWhileExecuting:@selector(myProgressTask) onTarget:self withObject:nil animated:YES];
        }else{
            matches = objects[0];
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            NSManagedObject *device = [self.devices objectAtIndex:0];
            NSString*urlString= [NSString stringWithFormat:@"%@",[device valueForKey:@"direct"]];
            NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            [[NSUserDefaults standardUserDefaults] setObject:url forKey:@"book_url"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSString * storyboardName = @"Main";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
            UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"READER"];
            [self presentViewController:vc animated:YES completion:nil];
        }
    }
}

//-(void)test{
//    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
//    [self.navigationController.view addSubview:HUD];
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
//
//    NSURL *URL = [NSURL URLWithString:@"http://www.kokzhiek.kz/books/1465286576-demo-maturidi_tandauly_frgm.epub"];
//    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
//    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
//        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
//        return [documentsDirectoryURL URLByAppendingPathComponent:@"1"];
//    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
//
//        NSLog(@"File downloaded to: %@", filePath);
//        EPubViewController *epubViewController = [[EPubViewController alloc]init];
//        [epubViewController loadEpub:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"1465286576-demo-maturidi_tandauly_frgm" ofType:@"epub"]]];
//        [self presentViewController:epubViewController animated:YES completion:^{
//            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//        }];
//        
//        
//    }];
//    
//    [downloadTask resume];
//    
//    HUD.mode = MBProgressHUDModeDeterminateHorizontalBar;
//    
//    HUD.delegate = self;
//    
//    [HUD showWhileExecuting:@selector(myProgressTask) onTarget:self withObject:nil animated:YES];
//    
//}

- (void)myProgressTask {
    float progress = 0.0f;
    while (progress < 1.0f) {
        progress += 0.01f;
        HUD.progress = progress;
        usleep(50000);
    }
}
- (void) showWithBlock:(MMPopupBlock)block{
    
}
- (IBAction)buyBookAction:(UIButton *)sender {
    NSInteger tag = sender.tag;
    
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    if([user objectForKey:@"email"] !=nil && ![[user objectForKey:@"email"] isEqualToString:@""]){
        
        NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
        NSString*password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
        
        NSString*url=@"";
        
        if(tag == 1){
            url=[NSString stringWithFormat:@"%@/kk/mobile/basket/add?book_id=%@&format=%@&quantity=%@&email=%@&password=%@",kBaseURL,self.book_id,@"paper",@"1",email,password];
            
            
        }else{
            
            
            NSArray *items =
             @[MMItemMake(NSLocalizedString(@"BUY", @""), MMItemTypeNormal, ^(NSInteger i){
                 NSLog(@"%@",url);
                 [SVProgressHUD show];
                 NSString *url=[NSString stringWithFormat:@"%@/kk/mobile/basket/add?book_id=%@&format=%@&quantity=%@&email=%@&password=%@",kBaseURL,self.book_id,@"ebook",@"1",email,password];
                 url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                 AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
                 [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                     
                     [SVProgressHUD dismiss];
                     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                     hud.mode = MBProgressHUDModeText;
                     hud.labelText = NSLocalizedString(@"ADDED_TO_CART", @"");
                     hud.margin = 10.f;
                     hud.yOffset = 150.f;
                     hud.removeFromSuperViewOnHide = YES;
                     
                     [hud hide:YES afterDelay:2];
                 } failure:^(NSURLSessionTask *operation, NSError *error) {
                     NSLog(@"Error: %@", error);
                     [SVProgressHUD dismiss];
                 }];
              }),
              MMItemMake(NSLocalizedString(@"PRESENT", @""), MMItemTypeNormal, ^(NSInteger i){
                  [self performSegueWithIdentifier:@"gift" sender:self];
              }),
              MMItemMake(NSLocalizedString(@"CLOSE", @""), MMItemTypeHighlight, nil)];
            
            [[[MMAlertView alloc] initWithTitle:NSLocalizedString(@"BOOK_BUY", @"")
                                         detail:self.book_title
                                          items:items]
            showWithBlock:nil];
            return;
        }
        
        NSLog(@"%@",url);
        [SVProgressHUD show];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            //collection_data= [responseObject objectForKey:@"1"];
            [SVProgressHUD dismiss];
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.labelText = NSLocalizedString(@"ADDED_TO_CART", @"");
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.removeFromSuperViewOnHide = YES;
            
            [hud hide:YES afterDelay:2];
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [SVProgressHUD dismiss];
        }];
    }
    else {
       /* MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = NSLocalizedString(@"NEED_AUTH", @"");
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        
        [hud hide:YES afterDelay:3];*/
        [self performSegueWithIdentifier:@"sign" sender:self];
    }
}

@end
