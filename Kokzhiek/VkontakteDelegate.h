//
//  VkontakteDelegate.h
//  Kokzhiek
//
//  Created by Admin on 08.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface VkontakteDelegate : NSObject

@property NSString *username, *realName, *ID, *link, *email, *access_token;
@property UIImage* photo;

+ (id)sharedInstance;
-(void) loginWithParams: (NSMutableDictionary*) params;
//-(void) postToWall;

@end