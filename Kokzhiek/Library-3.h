//
//  Library-3.h
//  Kokzhiek
//
//  Created by bugingroup on 07.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLSwipeContainerController.h"
@interface Library_3 : UICollectionViewController<XLSwipeContainerChildItem,UICollectionViewDataSource,UICollectionViewDelegate>

@end
