//
//  Registration.m
//  Kokzhiek
//
//  Created by bugingroup on 13.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "Registration.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
@interface Registration ()

@end

@implementation Registration

- (void)viewDidLoad {
    [super viewDidLoad];
    _email.delegate=self;
    _password.delegate=self;
    _return_password.delegate=self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField==self.email){
        [self.password becomeFirstResponder];
    }
    else   if (theTextField==self.password){
        [self.return_password becomeFirstResponder];
    }
    else  {
        [theTextField resignFirstResponder];
    }
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}

- (IBAction)registration:(id)sender {
    //NSString *email = _email.text;
    NSString *pass = _password.text;
    NSString *passA = _return_password.text;
    [self.view endEditing:true];
    if (![_email.text isEqualToString:@""]&&![_password.text isEqualToString:@""]&&![_return_password.text isEqualToString:@""]){
        NSString*url=[NSString stringWithFormat:@"http://kokzhiek.kz/kk/mobile/register?email=%@&password=%@&re_password=%@",_email.text,_password.text,_return_password.text];
        [SVProgressHUD show];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            
            [SVProgressHUD dismiss];
            
            NSLog(@"**+++%@",responseObject);
            
            
            //NSNumber * result = (NSNumber *)[responseObject objectForKey: @"result"];
            
            if([responseObject[@"status"] isEqualToString:@"success"]){
                [[NSUserDefaults standardUserDefaults] setObject:_email.text forKey:@"email"];
                [[NSUserDefaults standardUserDefaults] setObject:_password.text forKey:@"password"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
//                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//                hud.mode = MBProgressHUDModeText;
//                hud.labelText = @"Успешно";
//                hud.margin = 10.f;
//                hud.yOffset = 150.f;
//                hud.removeFromSuperViewOnHide = YES;
//                [hud hide:YES afterDelay:3];
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"Profile"];
                [[self navigationController] pushViewController:viewController animated:YES];
                
//                [self performSegueWithIdentifier:@"success" sender:self];
            }else{
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.labelText = NSLocalizedString(@"WRONG_DATA", @"");
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;
                [hud hide:YES afterDelay:2];
            }
            
//            if([result boolValue] != YES){
//                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//                hud.mode = MBProgressHUDModeText;
//                hud.labelText = @"Неверные данные";
//                hud.margin = 10.f;
//                hud.yOffset = 150.f;
//                hud.removeFromSuperViewOnHide = YES;
//                  [hud hide:YES afterDelay:3];
//                }
//            else {
//              
//                [[NSUserDefaults standardUserDefaults] setObject:_email.text forKey:@"email"];
//                [[NSUserDefaults standardUserDefaults] setObject:_password.text forKey:@"password"];
//                [[NSUserDefaults standardUserDefaults] synchronize];
//                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//                hud.mode = MBProgressHUDModeText;
//                hud.labelText = @"Успешно";
//                hud.margin = 10.f;
//                hud.yOffset = 150.f;
//                hud.removeFromSuperViewOnHide = YES;
//                
//                [hud hide:YES afterDelay:3];
//                [self performSegueWithIdentifier:@"success" sender:self];
//            }
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [SVProgressHUD dismiss];
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.labelText = NSLocalizedString(@"EMAIL_EXISTS", @"");
            hud.margin = 10.f;
            hud.yOffset = 150.f;
            hud.removeFromSuperViewOnHide = YES;
            [hud hide:YES afterDelay:2];
            
        }];
        
    }else{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = NSLocalizedString(@"PLEASE_FILL_IN", @"");
        if(pass != passA && ![pass isEqualToString:@""] && ![passA isEqualToString:@""]){
            hud.labelText = NSLocalizedString(@"PASSWORDS_DIFFER", @"");
        }
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
}
@end
