//
//  ChangeLanguage.m
//  Kokzhiek
//
//  Created by Admin on 04.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "ChangeLanguage.h"
#import "MBProgressHUD.h"

@interface ChangeLanguage ()

@end

@implementation ChangeLanguage

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *img = [UIImage imageNamed:@"fon"];
    UIImageView *imgV = [[UIImageView alloc] initWithImage:img];
    imgV.contentMode = UIViewContentModeScaleAspectFill;
    self.tableView.backgroundView = imgV;
    
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.layoutMargins = UIEdgeInsetsZero;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.cell1.layoutMargins = UIEdgeInsetsZero;
    self.cell1.preservesSuperviewLayoutMargins = false;
    self.cell2.layoutMargins = UIEdgeInsetsZero;
    self.cell2.preservesSuperviewLayoutMargins = false;
    
//    NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
//    NSLocale *locale = [NSLocale currentLocale];
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if ([language isEqualToString:@"kk"]){
       self.cell1.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else{
      self.cell2.accessoryType = UITableViewCellAccessoryCheckmark;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.row == 0){
        self.cell1.accessoryType = UITableViewCellAccessoryCheckmark;
        self.cell2.accessoryType = UITableViewCellAccessoryNone;
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"kk", nil] forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else{
        self.cell2.accessoryType = UITableViewCellAccessoryCheckmark;
        self.cell1.accessoryType = UITableViewCellAccessoryNone;
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"ru", nil] forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = NSLocalizedString(@"RESTART_APP", @"");
    hud.margin = 10.f;
    hud.yOffset = 150.f;
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hide:YES afterDelay:2];
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
