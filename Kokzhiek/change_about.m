//
//  change_about.m
//  Kokzhiek
//
//  Created by bugingroup on 23.06.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "change_about.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "MBProgressHUD.h"
#import "constants.h"
@interface change_about ()

@end

@implementation change_about

- (void)viewDidLoad {
    [super viewDidLoad];
    self.name.delegate=self;
    self.address.delegate=self;
    self.phone.delegate=self;
    NSString*name=[[NSUserDefaults standardUserDefaults]objectForKey:@"name"];
    NSString*address=[[NSUserDefaults standardUserDefaults]objectForKey:@"address"];
    NSString*phone=[[NSUserDefaults standardUserDefaults]objectForKey:@"phone"];
    self.name.text=name;
    self.address.text=address;
    self.phone.text=phone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField==self.name){
        [self.address becomeFirstResponder];
    }
    else   if (theTextField==self.address){
    [self.phone becomeFirstResponder];
    }
    else  {
        [theTextField resignFirstResponder];
    }
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)done:(id)sender {
    [self.view endEditing:true];
//    if (![self.phone.text isEqualToString:@""]&&![self.address.text isEqualToString:@""]&&![self.name.text isEqualToString:@""]){
               NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
               NSString*password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
        NSString*url=[NSString stringWithFormat:@"%@/kk/mobile/user/edit?name=%@&phone=%@&address=%@&email=%@&password=%@",kBaseURL,self.name.text,self.phone.text,self.address.text,email,password];
        [SVProgressHUD show];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            NSLog(@"**+++%@",responseObject);
            [SVProgressHUD dismiss];
            [_phone resignFirstResponder];
            [_address resignFirstResponder];
            [_name resignFirstResponder];
            int value = [[responseObject objectForKey:@"result"] intValue];
            if (value == 1) {
                [[NSUserDefaults standardUserDefaults] setObject:_address.text forKey:@"address"];
                [[NSUserDefaults standardUserDefaults] setObject:_name.text forKey:@"name"];
                [[NSUserDefaults standardUserDefaults] setObject:_phone.text forKey:@"phone"];;
                [[NSUserDefaults standardUserDefaults] synchronize];
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.labelText = NSLocalizedString(@"SAVED", @"");
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;
                
                [hud hide:YES afterDelay:2];
            }
            else {
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.labelText = NSLocalizedString(@"WRONG_DATA", @"");
                hud.margin = 10.f;
                hud.yOffset = 150.f;
                hud.removeFromSuperViewOnHide = YES;
                
                [hud hide:YES afterDelay:2];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [SVProgressHUD dismiss];
        }];
        
//    }
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}
@end
