//
//  SocialAuth.h
//  Kokzhiek
//
//  Created by Admin on 09.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialAuth : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtFio;
@property NSString *Id;
@property NSString *Provider;
@property NSString *Email;
@property NSString *Name;
@end
