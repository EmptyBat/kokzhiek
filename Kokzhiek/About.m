//
//  AboutMain.m
//  Kokzhiek
//
//  Created by Admin on 04.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "About.h"
#import "SWRevealViewController.h"

@interface About ()

@end

@implementation About

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _menu.target = self.revealViewController;
    _menu.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    UIImage *img = [UIImage imageNamed:@"fon"];
    UIImageView *imgV = [[UIImageView alloc] initWithImage:img];
    imgV.contentMode = UIViewContentModeScaleAspectFill;
    self.tableView.backgroundView = imgV;

    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.layoutMargins = UIEdgeInsetsZero;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.cell1.layoutMargins = UIEdgeInsetsZero;
    self.cell1.preservesSuperviewLayoutMargins = false;
    self.cell2.layoutMargins = UIEdgeInsetsZero;
    self.cell2.preservesSuperviewLayoutMargins = false;
    self.cell3.layoutMargins = UIEdgeInsetsZero;
    self.cell3.preservesSuperviewLayoutMargins = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)aboutAction:(id)sender {
}

@end
