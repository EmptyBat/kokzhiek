//
//  BookRead.m
//  Kokzhiek
//
//  Created by Admin on 16.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "BookRead.h"
#import "KFViewController.h"
#import "KFEpubController.h"
#import "KFEpubContentModel.h"
#import "EpubChapters.h"

@interface BookRead ()<KFEpubControllerDelegate, UIGestureRecognizerDelegate>
@property (nonatomic, strong) KFEpubController *epubController;

@property (nonatomic, strong) KFEpubContentModel *contentModel;

@property (nonatomic) NSUInteger spineIndex;
@end

@implementation BookRead

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = self.tl;
    
    
    NSURL *epubURL = self.path;//[[NSBundle mainBundle] URLForResource:self.path withExtension:@"epub"];
    
    NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
    self.epubController = [[KFEpubController alloc] initWithEpubURL:epubURL andDestinationFolder:documentsURL];
    self.epubController.delegate = self;
    
    
    
    [self.epubController openAsynchronous:YES];
    
    UISwipeGestureRecognizer *swipeRecognizer;
    swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeRight:)];
    swipeRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    swipeRecognizer.delegate = self;
    [self.wv addGestureRecognizer:swipeRecognizer];
    
    swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeLeft:)];
    swipeRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    swipeRecognizer.delegate = self;
    [self.wv addGestureRecognizer:swipeRecognizer];
    self.wv.delegate = self;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"Current URL = %@",webView.request.URL);
    //NSString *yourHTMLSourceCodeString = [webView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML"];
    //-- Add further custom actions if needed
}

- (void)didSwipeRight:(UIGestureRecognizer *)recognizer
{
    if (self.spineIndex > 1)
    {
        self.spineIndex--;
        [self updateContentForSpineIndex:self.spineIndex];
    }
}


- (void)didSwipeLeft:(UIGestureRecognizer *)recognizer
{
    if (self.spineIndex < self.contentModel.spine.count)
    {
        self.spineIndex++;
        [self updateContentForSpineIndex:self.spineIndex];
    }
}


#pragma mark Epub Contents


- (void)updateContentForSpineIndex:(NSUInteger)currentSpineIndex
{
    NSString *contentFile = self.contentModel.manifest[self.contentModel.spine[currentSpineIndex]][@"href"];
    NSURL *contentURL = [self.epubController.epubContentBaseURL URLByAppendingPathComponent:contentFile];
    NSLog(@"content URL :%@", contentURL);
    
    NSString* content = [NSString stringWithContentsOfFile:contentURL
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    
    NSString *html = @"<style>body{background-color:red;}</style>";
    html = @"";
    
    content = [content stringByReplacingOccurrencesOfString:@"</head>"
                                                 withString:[NSString stringWithFormat:@"%@%@",html,@"</head>" ] ];
    
    //NSURLRequest *request = [[NSURLRequest alloc] initWithURL:contentURL];
    [self.wv loadHTMLString:content baseURL:nil];
    //[self.webView loadHTMLString:content];
}


#pragma mark KFEpubControllerDelegate Methods


- (void)epubController:(KFEpubController *)controller willOpenEpub:(NSURL *)epubURL
{
    NSLog(@"will open epub");
}


- (void)epubController:(KFEpubController *)controller didOpenEpub:(KFEpubContentModel *)contentModel
{
    NSLog(@"opened: %@", contentModel.metaData[@"title"]);
    self.contentModel = contentModel;
    self.spineIndex = 1;
    [self updateContentForSpineIndex:self.spineIndex];
}


- (void)epubController:(KFEpubController *)controller didFailWithError:(NSError *)error
{
    NSLog(@"epubController:didFailWithError: %@", error.description);
}


#pragma mark - UIGestureRecognizerDelegate Methods


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}
- (IBAction)showChapters:(id)sender {
//    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"EpubChapters"];
//    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    
//    
//    [self presentViewController:vc animated:YES completion:NULL];
    
}
- (IBAction)showStyle:(id)sender {
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"chapters"]) {
        EpubChapters *vc = segue.destinationViewController;
        vc.chapters = @[@"Ch1",@"Ch2"];
    }
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
}
@end