//
//  Pickup.h
//  Kokzhiek
//
//  Created by Admin on 07.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Pickup : UIViewController

@property NSString *txtFio;
@property NSString *txtEmail;
@property NSString *txtPhone;
@property NSString *txtCity;

@end
