//
//  Gift.m
//  Kokzhiek
//
//  Created by Admin on 09.07.16.
//  Copyright © 2016 Almas. All rights reserved.
//

#import "Gift.h"
#import "Books_collection.h"
#import "AFNetworking.h"
#import <TLYShyNavBar/TLYShyNavBarManager.h>
#import "SWRevealViewController.h"
#import "SVProgressHUD.h"
#import "constants.h"
#import "ORGContainerCellView.h"
#import "ORGContainerCell.h"
#import "ORGArticleCollectionViewCell.h"
#import "BookDetail.h"
#import "EPubViewController.h"
#import "MBProgressHUD.h"
#import "FTWCache.h"
#import "NSString+MD5.h"
#import "BMEpayViewController.h"
@interface Gift ()

@end

@implementation Gift

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.txtEmail.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)giftAction:(id)sender {
    
    if (![self.txtEmail.text isEqualToString:@""]&&![self.txtEmail.text isEqualToString:@""]){
        NSString*email=[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
        NSString*password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
        
        NSString*url=[NSString stringWithFormat:@"%@kk/mobile/addgift?myemail=%@&password=%@&email=%@&ebook_price=%@&book_id=%@",kBaseURL,email,password,self.txtEmail.text,self.price
                      ,self.bookId];
        NSLog(@"%@",url);
        [SVProgressHUD show];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            //collection_data= [responseObject objectForKey:@"1"];
            [SVProgressHUD dismiss];
            
            NSString *b64 = [responseObject valueForKey:@"content"];
            BMEpayViewController *controller = [[BMEpayViewController alloc] init];
            controller.delegate = self;
            controller.sdkMode = EpaySdkModeProduction;
            controller.signedOrderBase64 = b64;
            controller.postLink = @"http://kokzhiek.kz/user/test";
            //controller.template = @"besmart_android.xsl";
            controller.clientEmail = self.txtEmail.text;
            controller.language = EpayLanguageRussian;
            
            [self presentViewController:controller animated:YES completion:^{
                
            }];
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [SVProgressHUD dismiss];
        }];
    }else{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = NSLocalizedString(@"PLEASE_FILL_IN", @"");
        hud.margin = 10.f;
        hud.yOffset = 150.f;
        hud.removeFromSuperViewOnHide = YES;
        [hud hide:YES afterDelay:2];
    }
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
-(void)epayPaymentSuccededWithController:(BMEpayViewController*)epayViewController {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = NSLocalizedString(@"PAYED", @"");
    hud.margin = 10.f;
    hud.yOffset = 150.f;
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hide:YES afterDelay:2];
    [self.navigationController popToRootViewControllerAnimated:true];
    NSLog(@"SUCCESS");
}

-(void)epayPaymentFailedWithController:(BMEpayViewController*)epayViewController {
    NSLog(@"FAIL");
}

-(void)epayPaymentCancelledWithController:(BMEpayViewController*)epayViewController {
    NSLog(@"CANCEL");
}
@end
